package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HalamanUtama extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG_RESULT = "result";
    private static final String TAG_INDOOR = "indoor";
    private static final String TAG_OUTDOOR = "outdoor";
    private static final String TAG_IDLOKASI = "idlokasi";
    private static final String TAG_NAMALOKASI = "namalokasi";
    private static final String TAG_GAMBAR = "gambar";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterListLokasi mGridAdapter;
    private ArrayList<GridItemLisLokasi> mGridData;

    TableLayout layoutindooroutdoor;

    private static final String TAG = HalamanUtama.class.getSimpleName();

    String oldresponse;

    String response;

    ImageView imgindoor,imgoutdoor;
    TextView indoorbook,outdoorbook;
    TextView indoortext,outdoortext,tutup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_utama);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        tutup = (TextView) findViewById(R.id.tutup);
        indoorbook = (TextView) findViewById(R.id.indoorbook);
        indoortext = (TextView) findViewById(R.id.indoortext);
        outdoorbook = (TextView) findViewById(R.id.outdoorbook);
        outdoortext = (TextView) findViewById(R.id.outdoortext);
        imgindoor = (ImageView) findViewById(R.id.imgindoor);
        imgoutdoor = (ImageView) findViewById(R.id.imgoutdoor);
        layoutindooroutdoor = (TableLayout) findViewById(R.id.layoutindooroutdoor);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header= navigationView.getHeaderView(0);
        TextView nama=(TextView) header.findViewById(R.id.nama);
        TextView email=(TextView) header.findViewById(R.id.email);
        nama.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("nama", ""));
        email.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("email", ""));

        mGridView = (GridView) findViewById(R.id.gridView);

        FEED_URL = Config.MAP_URL;

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterListLokasi(HalamanUtama.this, R.layout.grid_item_layout_list_lokasi, mGridData);
        mGridView.setAdapter(mGridAdapter);

        pd = new ProgressDialog(HalamanUtama.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        //Start download
        new getLokasi().execute(FEED_URL);

        layoutindooroutdoor.setVisibility(View.GONE);

        tutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutindooroutdoor.setVisibility(View.GONE);
            }
        });

        imgindoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("kategori", "indoor");
                editor.commit();

                startActivity(new Intent(HalamanUtama.this,GambarFasilitas.class));
            }
        });

        imgoutdoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("kategori", "outdoor");
                editor.commit();

                startActivity(new Intent(HalamanUtama.this,GambarFasilitas.class));
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemLisLokasi item = (GridItemLisLokasi) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idlokasi", item.getIdlokasi());
                editor.putString("namalokasi", item.getNamalokasi());
                editor.commit();

                new setIndoorOutdoor().execute();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_kelola_akun) {
            startActivity(new Intent(HalamanUtama.this,KelolaAkun.class));
        } else if (id == R.id.nav_cari_lokasi) {
            startActivity(new Intent(HalamanUtama.this,CariLokasi.class));
        } else if (id == R.id.nav_reservasi) {
            startActivity(new Intent(HalamanUtama.this,ListPemesanan.class));
        } else if (id == R.id.nav_melihat_jadwal) {
            startActivity(new Intent(HalamanUtama.this,ListLokasi.class));
        } else if (id == R.id.nav_cara_pemesanan) {
            startActivity(new Intent(HalamanUtama.this,CaraPemesanan.class));
        } else if (id == R.id.nav_keluar) {
            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
            editor.clear();
            editor.commit();

            startActivity(new Intent(HalamanUtama.this,Login.class));
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class setIndoorOutdoor extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.MAPINDOOROUTDOOR_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");;

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String indoor = a.getString(TAG_INDOOR);
                            String outdoor = a.getString(TAG_OUTDOOR);

                            if(!indoor.equals("")){
                                byte[] decodedString1 = Base64.decode(indoor, Base64.DEFAULT);
                                Bitmap decodedByte1 = BitmapFactory.decodeByteArray(decodedString1, 0, decodedString1.length);
                                imgindoor.setImageBitmap(Bitmap.createScaledBitmap(decodedByte1, 150, 150, false));
                                indoortext.setVisibility(View.VISIBLE);
                                imgindoor.setVisibility(View.VISIBLE);
                                indoorbook.setVisibility(View.VISIBLE);
                            }
                            else{
                                indoortext.setVisibility(View.INVISIBLE);
                                imgindoor.setVisibility(View.INVISIBLE);
                                indoorbook.setVisibility(View.INVISIBLE);
                            }

                            if(!outdoor.equals("")){
                                byte[] decodedString2 = Base64.decode(outdoor, Base64.DEFAULT);
                                Bitmap decodedByte2 = BitmapFactory.decodeByteArray(decodedString2, 0, decodedString2.length);
                                imgoutdoor.setImageBitmap(Bitmap.createScaledBitmap(decodedByte2, 150, 150, false));
                                outdoortext.setVisibility(View.VISIBLE);
                                imgoutdoor.setVisibility(View.VISIBLE);
                                outdoorbook.setVisibility(View.VISIBLE);
                            }
                            else{
                                outdoortext.setVisibility(View.INVISIBLE);
                                imgoutdoor.setVisibility(View.INVISIBLE);
                                outdoorbook.setVisibility(View.INVISIBLE);
                            }


                            SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                            editor.putString("indoor", indoor);
                            editor.putString("outdoor", outdoor);
                            editor.commit();

                            if (indoor.equals("")&&outdoor.equals("")){
                                layoutindooroutdoor.setVisibility(View.GONE);
                                Toast.makeText(HalamanUtama.this, "Pengelola belum menambahkan fasilitas", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                layoutindooroutdoor.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                    else{
                        layoutindooroutdoor.setVisibility(View.GONE);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(HalamanUtama.this);
                        builder1.setMessage("Maaf pengelola belum menambahkan fasilitas lokasi");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                    pd.hide();
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }

    //Downloading data asynchronously
    public class getLokasi extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    response = streamToString(httpResponse.getEntity().getContent());

                    parseResult(response);

                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemLisLokasi item;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String stridlokasi = post.getString(TAG_IDLOKASI);
                String strnamalokasi = post.getString(TAG_NAMALOKASI);
                item = new GridItemLisLokasi();
                item.setIdlokasi(stridlokasi);
                item.setNamalokasi(strnamalokasi);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString(TAG_GAMBAR));
                }

                mGridData.add(item);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
