package ce.studio.btl;

public class Config {

    public static String MAP_URL="http://192.168.23.1/btl/map.php";
    public static String MAPLOKASIPENGGUNA_URL="http://192.168.23.1/btl/maplokasipengguna.php";
    public static String CARIMAP_URL="http://192.168.23.1/btl/carimap.php";
    public static String MAPINDOOROUTDOOR_URL="http://192.168.23.1/btl/mapindooroutdoor.php";
    public static String DETAILMAPINDOOROUTDOOR_URL="http://192.168.23.1/btl/detail.php";
    public static String DETAILPEMESANAN_URL="http://192.168.23.1/btl/detailpemesanan.php";
    public static String DETAILLOKASIPENGELOLA_URL="http://192.168.23.1/btl/detaillokasipengelola.php";
    public static String DETAILINDOORPENGELOLA_URL="http://192.168.23.1/btl/detailindoorpengelola.php";
    public static String DETAILOUTDOORRPENGELOLA_URL="http://192.168.23.1/btl/detailoutdoorpengelola.php";
    public static String LISTPEMESANAN_URL="http://192.168.23.1/btl/listpemesanan.php";
    public static String LISTPEMESANANPENGELOLA_URL="http://192.168./btl/listpemesananpengelola.php";
    public static String DATAAKUN_URL="http://192.168.23.1/btl/dataakun.php";
    public static String SIMPANAKUN_URL="http://192.168.23.1/btl/simpanakun.php";
    public static String TAMBAHLOKASIPENGELOLA_URL="http://192.168.23.1/btl/tambahlokasipengelola.php";
    public static String TAMBAHINDOOROUTDOOR_URL="http://192.168.23.1/btl/tambahindooroutdoorpengelola.php";
    public static String UBAHINDOOROUTDOOR_URL="http://192.168.23.1/btl/ubahindooroutdoorpengella.php";
    public static String UBAHLOKASIGALERI_URL="http://192.168.23.1/btl/ubahlokasigaleri.php";
    public static String GALERY_URL="http://192.168.23.1/btl/galery.php";
    public static String KALENDER_URL="http://192.168.23.1/btl/kalender.php";
    public static String BUATPESANAN_URL="http://192.168.23.1/btl/buatpesanan.php";
    public static String LOGIN_URL="http://192.168.23.1/btl/login.php";
    public static String HAPUS_URL="http://192.168.23.1/btl/deletepemesanan.php";
    public static String REGISTER_URL="http://192.168.23.1/btl/register.php";
    public static String KIRIMBUKTI_URL="http://192.168.23.1/btl/kirimbukti.php";
    public static String KIRIMKOMENTAR_URL="http://192.168.23.1/btl/kirimkomentar.php";
    public static String KONFIRMASIPEMESANAN_URL="http://192.168.23.1/btl/konfirmasipemesanan.php";
    public static String LAPORANGRAFIK_URL="http://192.168.23.1/btl/laporangrafik.php";
    public static String LISTKOMENTAR_URL="http://192.168.23.1/btl/listkomentar.php";
    public static String LUPAPASSWORD="http://192.168.23.1/btl/lupapassword.php";


}
