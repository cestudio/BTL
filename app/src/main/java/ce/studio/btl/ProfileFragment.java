package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends Fragment {

    Button btnubahakun;

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_ALAMAT = "alamat";
    private static final String TAG_TELP = "telp";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_PASSWORD = "password";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    TextView alamat,nomorhp,username,password,nama,email;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);

        nama = (TextView) rootview.findViewById(R.id.nama);
        email = (TextView) rootview.findViewById(R.id.email);
        alamat = (TextView) rootview.findViewById(R.id.alamat);
        nomorhp = (TextView) rootview.findViewById(R.id.nomorhp);
        username = (TextView) rootview.findViewById(R.id.username);
        password = (TextView) rootview.findViewById(R.id.password);
        btnubahakun = (Button) rootview.findViewById(R.id.btnubahakun);

        new dataakun().execute();

        btnubahakun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new UbahAkunFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.pertama,fragment);
                ft.commit();
            }
        });

        return rootview;
    }

    private class dataakun extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.DATAAKUN_URL;
            FEED_URL += "?iduser="+getActivity().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","");

            Log.e("Profile =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strnama = a.getString(TAG_NAMA);
                        String stremail = a.getString(TAG_EMAIL);
                        String stralamat = a.getString(TAG_ALAMAT);
                        String strtelp = a.getString(TAG_TELP);
                        String strusername = a.getString(TAG_USERNAME);
                        String strpassword = a.getString(TAG_PASSWORD);

                        nama.setText(strnama);
                        email.setText(stremail);
                        alamat.setText(stralamat);
                        nomorhp.setText(strtelp);
                        username.setText(strusername);
                        password.setText(strpassword);
                    }
                    pd.hide();
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }
}