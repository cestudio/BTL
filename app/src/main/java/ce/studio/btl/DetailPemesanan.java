package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import ce.studio.btl.Galeri.RequestHandler;


public class DetailPemesanan extends AppCompatActivity {

    ImageView sliderLayout;
    HashMap<String,String> Hash_file_maps ;

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_GAMBAR = "gambar";

    private static final String TAG_NAMAPEMILIK = "namapemilik";
    private static final String TAG_JUMLAHHARGA = "jumlahharga";
    private static final String TAG_NAMALOKASI = "namalokasi";
    private static final String TAG_TIPELOKASI = "tipelokasi";
    private static final String TAG_JENISLOKASI = "jenislokasi";
    private static final String TAG_HARGALOKASI = "hargalokasi";
    private static final String TAG_NOMORPEMESANAN = "nomorpemesanan";
    private static final String TAG_LAMABOOKING = "lamabooking";
    private static final String TAG_JUMLAHRUANGAN = "jumlahruangan";
    private static final String TAG_DISKON = "diskon";
    private static final String TAG_STATUSPEMESANAN = "statuspemesanan";
    private static final String TAG_GAMBARLOKASI = "gambarlokasi";
    private static final String TAG_STATUSBUTTON = "statusbutton";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    Button btnbooknow;

    TextView jenislokasi,namapemilik,jumlahharga,namalokasi,tipelokasi,hargalokasi,nomorpemesanan,lamabooking,jumlahruangan,diskon,statuspemesanan,transferpayment;

    ImageView gambar;

    ProgressDialog pd;

    private Uri fileUri1;

    private int PICK_IMAGE_REQUEST = 1;

    private Bitmap bitmap1;

    String strimage1;

    private String filePath = null;

    private static final String TAG = DetailPemesanan.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_pemesanan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        namapemilik = (TextView) findViewById(R.id.namapemilik);
        jumlahharga = (TextView) findViewById(R.id.jumlahharga);
        namalokasi = (TextView) findViewById(R.id.namalokasi);
        tipelokasi = (TextView) findViewById(R.id.tipelokasi);
        jenislokasi = (TextView) findViewById(R.id.jenislokasi);
        hargalokasi = (TextView) findViewById(R.id.hargalokasi);
        nomorpemesanan = (TextView) findViewById(R.id.nomorpemesanan);
        lamabooking = (TextView) findViewById(R.id.lamabooking);
        jumlahruangan = (TextView) findViewById(R.id.jumlahruangan);
//        diskon = (TextView) findViewById(R.id.diskon);
        statuspemesanan = (TextView) findViewById(R.id.statuspemesanan);
        transferpayment = (TextView) findViewById(R.id.transferpayment);
        gambar = (ImageView) findViewById(R.id.gambar);

        new getDetailPemesanan().execute();

        transferpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(transferpayment.getText().toString().equals("Beri Nilai Lokasi!")){
                    Komentar k = new Komentar(DetailPemesanan.this);
                    k.show();
                }
                else{
                    isStoragePermissionGrantedGaleri1();
                }
            }
        });

        Hash_file_maps = new HashMap<String, String>();

        sliderLayout = (ImageView)findViewById(R.id.slider);

    }

    public  boolean isStoragePermissionGrantedGaleri1() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                showFileChooser1();
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            showFileChooser1();
            return true;
        }
    }

    public void uploadImageGaleri(){

        if (fileUri1!=null){
            strimage1 = getStringImage(bitmap1);
        }

        class uploadImageGaleri extends AsyncTask<Void,Void,String>{
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = ProgressDialog.show(DetailPemesanan.this,"Tunggu sebentar.....","Mengunggah",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pd.dismiss();
                if(s.equals("0")){
                    Toast.makeText(DetailPemesanan.this, "Gagal menambah lokasi!", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(DetailPemesanan.this, "Sukses menambah lokasi!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> param = new HashMap<String,String>();
                param.put("idpemesanan",getSharedPreferences("DATA",MODE_PRIVATE).getString("idpemesanan",""));

                if (strimage1!=null){
                    param.put("image1",strimage1);
                }

                String result = rh.sendPostRequest(Config.KIRIMBUKTI_URL, param);

                return result;
            }
        }
        uploadImageGaleri u = new uploadImageGaleri();
        u.execute();
    }

    private class getDetailPemesanan extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(DetailPemesanan.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.DETAILPEMESANAN_URL;
            FEED_URL += "?idpemesanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idpemesanan","").replace(" ","%20");

            Log.e("Detail =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String strnamapemilik = a.getString(TAG_NAMAPEMILIK);
                            String strjumlahharga = a.getString(TAG_JUMLAHHARGA);
                            String strnamalokasi = a.getString(TAG_NAMALOKASI);
                            String strtipelokasi = a.getString(TAG_TIPELOKASI);
                            String strjenislokasi = a.getString(TAG_JENISLOKASI);
                            String strhargalokasi = a.getString(TAG_HARGALOKASI);
                            String strnomorpemesanan = a.getString(TAG_NOMORPEMESANAN);
                            String strlamabooking = a.getString(TAG_LAMABOOKING);
                            String strjumlahruangan = a.getString(TAG_JUMLAHRUANGAN);
//                            String strdiskon = a.getString(TAG_DISKON);
                            String strstatuspemesanan = a.getString(TAG_STATUSPEMESANAN);
                            String strgambar = a.getString(TAG_GAMBARLOKASI);
                            String strstatusbutton = a.getString(TAG_STATUSBUTTON);

                            namapemilik.setText(strnamapemilik);
                            jumlahharga.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strjumlahharga))).replace(",","."));
                            namalokasi.setText(strnamalokasi);
                            tipelokasi.setText(strtipelokasi);
                            jenislokasi.setText(strjenislokasi);
                            hargalokasi.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strhargalokasi))).replace(",",".")+" / 3 Jam");
                            nomorpemesanan.setText(strnomorpemesanan);
                            lamabooking.setText(strlamabooking + " jam");
                            jumlahruangan.setText(strjumlahruangan);
//                            diskon.setText(strdiskon);

                            byte[] decodedString1 = Base64.decode(strgambar, Base64.DEFAULT);
                            Bitmap decodedByte1 = BitmapFactory.decodeByteArray(decodedString1, 0, decodedString1.length);
                            gambar.setImageBitmap(Bitmap.createScaledBitmap(decodedByte1, 300, 300, false));

                            byte[] decodedString = Base64.decode(strgambar, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            sliderLayout.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, 500, 500, false));

//                            if(strstatuspemesanan.equals("00")){
//                                statuspemesanan.setText("Menunggu konfirmasi pengelola");
//                                transferpayment.setVisibility(View.GONE);
//                            }
                            if(strstatuspemesanan.equals("00")){
                                statuspemesanan.setText("Menunggu konfirmasi pembayaran");
                                transferpayment.setVisibility(View.VISIBLE);
                            }
                            else if(strstatuspemesanan.equals("01")){
                                statuspemesanan.setText("Menunggu proses verifikasi");
                                transferpayment.setVisibility(View.GONE);
                            }
                            else if(strstatuspemesanan.equals("11")){
                                statuspemesanan.setText("Proses pemesanan tempat disetujui");
                                transferpayment.setText("Beri Nilai Lokasi!");
                                if(strstatusbutton.equals("0")){
                                    transferpayment.setVisibility(View.VISIBLE);
                                }
                                else if(strstatusbutton.equals("1")){
                                    transferpayment.setVisibility(View.GONE);
                                }
                            }



                        }

                    }
                    else{
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DetailPemesanan.this);
                        builder1.setMessage("Mohon maaf detail pemesanan untuk "+getSharedPreferences("DATA",MODE_PRIVATE).getString("namalokasi","")+" tidak diketahui");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }


                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");

            }
            pd.hide();
        }

    }

    private void showFileChooser1() {
        Intent intent = new Intent();
        filePath = "1";
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Ambil Gambar"), PICK_IMAGE_REQUEST);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST){
            if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                if (filePath != null && filePath.equals("1")) {
                    fileUri1 = data.getData();
                    try {
                        bitmap1 = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri1);
                        uploadImageGaleri();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}