package ce.studio.btl;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.IOException;

public class PasswordBaru extends AppCompatActivity {

    public Button kirim;
    public EditText passwordbaru;

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_baru);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        passwordbaru = (EditText) findViewById(R.id.passwordbaru);
        kirim = (Button) findViewById(R.id.btnkirim);

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new ubahpassword().execute();
            }
        });
    }

    private class ubahpassword extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {

            pd = new ProgressDialog(PasswordBaru.this);
            pd.setMessage("Sedang mengubah password baru, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            try {

                FEED_URL = Config.LUPAPASSWORD;
                FEED_URL += "?username="+getSharedPreferences("DATA",MODE_PRIVATE).getString("username","").replace(" ","%20");
                FEED_URL += "&password="+passwordbaru.getText().toString().replace(" ","%20");

                Log.e("Ubah Password =",FEED_URL);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(FEED_URL);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }


            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {

            if(result.equals("1")){
                Toast.makeText(PasswordBaru.this, "Sukses mengubah password!", Toast.LENGTH_SHORT).show();
                finish();
                pd.hide();
            }
            else{
                Toast.makeText(PasswordBaru.this, "Gagal mengubah komentar!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }

            super.onPostExecute(result);
        }

    }

}
