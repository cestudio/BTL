package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class DetailIndoorOutdoorPengelola extends AppCompatActivity {

    TextView jumlahruanganindoor,jumlahruanganoutdoor,sisaruanganindoor,sisaruanganoutdoor,hargaindoor,hargaoutdoor,deskripsilokasi;
    TextView fasilitasindoor1,fasilitasindoor2,fasilitasindoor3,fasilitasindoor4,fasilitasindoor5,fasilitasindoor6,fasilitasindoor7,fasilitasindoor8,fasilitasindoor9,fasilitasindoor10;
    TextView fasilitasoutdoor1,fasilitasoutdoor2,fasilitasoutdoor3,fasilitasoutdoor4,fasilitasoutdoor5,fasilitasoutdoor6,fasilitasoutdoor7,fasilitasoutdoor8,fasilitasoutdoor9,fasilitasoutdoor10;
    ImageView gambarindoor,gambaroutdoor;
    Button btnubahinformasi;

    private static final String TAG_RESULT = "result";
    private static final String TAG_GAMBARINDOOR = "gambarindoor";
    private static final String TAG_HARGAINDOOR = "hargaindoor";
    private static final String TAG_JUMLAHRUANGANINDOOR = "jumlahruangindoor";
    private static final String TAG_SISARUANGANINDOOR= "sisaruangindoor";
    private static final String TAG_FASILITASINDOOR1= "fasilitasindoor1";
    private static final String TAG_FASILITASINDOOR2= "fasilitasindoor2";
    private static final String TAG_FASILITASINDOOR3= "fasilitasindoor3";
    private static final String TAG_FASILITASINDOOR4= "fasilitasindoor4";
    private static final String TAG_FASILITASINDOOR5= "fasilitasindoor5";
    private static final String TAG_FASILITASINDOOR6= "fasilitasindoor6";
    private static final String TAG_FASILITASINDOOR7= "fasilitasindoor7";
    private static final String TAG_FASILITASINDOOR8= "fasilitasindoor8";
    private static final String TAG_FASILITASINDOOR9= "fasilitasindoor9";
    private static final String TAG_FASILITASINDOOR10= "fasilitasindoor10";

    private static final String TAG_GAMBAROUTDOOR = "gambaroutdoor";
    private static final String TAG_HARGAOUTDOOR = "hargaoutdoor";
    private static final String TAG_JUMLAHRUANGANOUTDOOR = "jumlahruangoutdoor";
    private static final String TAG_SISARUANGANOUTDOOR= "sisaruangoutdoor";
    private static final String TAG_FASILITASOUTDOOR1= "fasilitasoutdoor1";
    private static final String TAG_FASILITASOUTDOOR2= "fasilitasoutdoor2";
    private static final String TAG_FASILITASOUTDOOR3= "fasilitasoutdoor3";
    private static final String TAG_FASILITASOUTDOOR4= "fasilitasoutdoor4";
    private static final String TAG_FASILITASOUTDOOR5= "fasilitasoutdoor5";
    private static final String TAG_FASILITASOUTDOOR6= "fasilitasoutdoor6";
    private static final String TAG_FASILITASOUTDOOR7= "fasilitasoutdoor7";
    private static final String TAG_FASILITASOUTDOOR8= "fasilitasoutdoor8";
    private static final String TAG_FASILITASOUTDOOR9= "fasilitasoutdoor9";
    private static final String TAG_FASILITASOUTDOOR10= "fasilitasoutdoor10";

    static boolean a=false;

    JSONArray rs1 = null;
    JSONArray rs2 = null;

    public static String FEED_URL;

    ProgressDialog pd;

    String strgambarindoor,strgambaroutdoor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gambar_fasilitas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        hargaindoor = (TextView) findViewById(R.id.hargaindoor);
        hargaoutdoor = (TextView) findViewById(R.id.hargaoutdoor);
        jumlahruanganindoor = (TextView) findViewById(R.id.jumlahruanganindoor);
        jumlahruanganoutdoor = (TextView) findViewById(R.id.jumlahruanganoutdoor);
        sisaruanganindoor = (TextView) findViewById(R.id.sisaruanganindoor);
        sisaruanganoutdoor = (TextView) findViewById(R.id.sisaruanganoutdoor);
        deskripsilokasi = (TextView) findViewById(R.id.deskripsilokasi);

        fasilitasindoor1 = (TextView) findViewById(R.id.fasilitasindoor1);
        fasilitasindoor2 = (TextView) findViewById(R.id.fasilitasindoor2);
        fasilitasindoor3 = (TextView) findViewById(R.id.fasilitasindoor3);
        fasilitasindoor4 = (TextView) findViewById(R.id.fasilitasindoor4);
        fasilitasindoor5 = (TextView) findViewById(R.id.fasilitasindoor5);
        fasilitasindoor6 = (TextView) findViewById(R.id.fasilitasindoor6);
        fasilitasindoor7 = (TextView) findViewById(R.id.fasilitasindoor7);
        fasilitasindoor8 = (TextView) findViewById(R.id.fasilitasindoor8);
        fasilitasindoor9 = (TextView) findViewById(R.id.fasilitasindoor9);
        fasilitasindoor10 = (TextView) findViewById(R.id.fasilitasindoor10);

        fasilitasoutdoor1 = (TextView) findViewById(R.id.fasilitasoutdoor1);
        fasilitasoutdoor2 = (TextView) findViewById(R.id.fasilitasoutdoor2);
        fasilitasoutdoor3 = (TextView) findViewById(R.id.fasilitasoutdoor3);
        fasilitasoutdoor4 = (TextView) findViewById(R.id.fasilitasoutdoor4);
        fasilitasoutdoor5 = (TextView) findViewById(R.id.fasilitasoutdoor5);
        fasilitasoutdoor6 = (TextView) findViewById(R.id.fasilitasoutdoor6);
        fasilitasoutdoor7 = (TextView) findViewById(R.id.fasilitasoutdoor7);
        fasilitasoutdoor8 = (TextView) findViewById(R.id.fasilitasoutdoor8);
        fasilitasoutdoor9 = (TextView) findViewById(R.id.fasilitasoutdoor9);
        fasilitasoutdoor10 = (TextView) findViewById(R.id.fasilitasoutdoor10);

        gambarindoor = (ImageView) findViewById(R.id.gambarindoor);
        gambaroutdoor = (ImageView) findViewById(R.id.gambaroutdoor);

        btnubahinformasi = (Button) findViewById(R.id.btnubahinformasi);

        btnubahinformasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailIndoorOutdoorPengelola.this,UbahIndoorOutdoorPengelola.class));
                finish();
            }
        });

        new getDetailIndoor().execute();
    }

    private class getDetailIndoor extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(DetailIndoorOutdoorPengelola.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.DETAILINDOORPENGELOLA_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");

            Log.e("Indoor =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs1 = json.getJSONArray(TAG_RESULT);
                    if(rs1.length()>0){
                        for(int i=0; i<rs1.length();i++)
                        {
                            JSONObject a = rs1.getJSONObject(i);
                            strgambarindoor = a.getString(TAG_GAMBARINDOOR);
                            String strhargaindoor = a.getString(TAG_HARGAINDOOR);
                            String strjumlahruangindoor = a.getString(TAG_JUMLAHRUANGANINDOOR);
                            String strsisaruanganindoor = a.getString(TAG_SISARUANGANINDOOR);
                            String strfasilitasindoor1 = a.getString(TAG_FASILITASINDOOR1);
                            String strfasilitasindoor2 = a.getString(TAG_FASILITASINDOOR2);
                            String strfasilitasindoor3 = a.getString(TAG_FASILITASINDOOR3);
                            String strfasilitasindoor4 = a.getString(TAG_FASILITASINDOOR4);
                            String strfasilitasindoor5 = a.getString(TAG_FASILITASINDOOR5);
                            String strfasilitasindoor6 = a.getString(TAG_FASILITASINDOOR6);
                            String strfasilitasindoor7 = a.getString(TAG_FASILITASINDOOR7);
                            String strfasilitasindoor8 = a.getString(TAG_FASILITASINDOOR8);
                            String strfasilitasindoor9 = a.getString(TAG_FASILITASINDOOR9);
                            String strfasilitasindoor10 = a.getString(TAG_FASILITASINDOOR10);


                            if(strhargaindoor.equals("")) {
                                hargaindoor.setText("");
                            }
                            else{
                                hargaindoor.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strhargaindoor))).replace(",","."));
                            }

                            jumlahruanganindoor.setText(strjumlahruangindoor);
                            sisaruanganindoor.setText(strsisaruanganindoor);
                            fasilitasindoor1.setText(strfasilitasindoor1);
                            fasilitasindoor2.setText(strfasilitasindoor2);
                            fasilitasindoor3.setText(strfasilitasindoor3);
                            fasilitasindoor4.setText(strfasilitasindoor4);
                            fasilitasindoor5.setText(strfasilitasindoor5);
                            fasilitasindoor6.setText(strfasilitasindoor6);
                            fasilitasindoor7.setText(strfasilitasindoor7);
                            fasilitasindoor8.setText(strfasilitasindoor8);
                            fasilitasindoor9.setText(strfasilitasindoor9);
                            fasilitasindoor10.setText(strfasilitasindoor10);

                            if(!strgambarindoor.equals("")){
                                byte[] decodedString = Base64.decode(strgambarindoor, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                gambarindoor.setImageBitmap(decodedByte);
                            }

                        }

                    }
                    else{
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DetailIndoorOutdoorPengelola.this);
                        builder1.setMessage("Mohon maaf anda harus menambahkan jenis fasilitas pada lokasi "+getSharedPreferences("DATA",MODE_PRIVATE).getString("namalokasi",""));
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                    new getDetailOutdoor().execute();
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");

            }
        }

    }

    private class getDetailOutdoor extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.DETAILOUTDOORRPENGELOLA_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");

            Log.e("Outdoor =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs2 = json.getJSONArray(TAG_RESULT);
                    if(rs2.length()>0){
                        for(int i=0; i<rs2.length();i++)
                        {
                            JSONObject a = rs2.getJSONObject(i);
                            strgambaroutdoor = a.getString(TAG_GAMBAROUTDOOR);
                            String strhargaoutdoor = a.getString(TAG_HARGAOUTDOOR);
                            String strjumlahruangoutdoor = a.getString(TAG_JUMLAHRUANGANOUTDOOR);
                            String strsisaruanganoutdoor = a.getString(TAG_SISARUANGANOUTDOOR);
                            String strfasilitasoutdoor1 = a.getString(TAG_FASILITASOUTDOOR1);
                            String strfasilitasoutdoor2 = a.getString(TAG_FASILITASOUTDOOR2);
                            String strfasilitasoutdoor3 = a.getString(TAG_FASILITASOUTDOOR3);
                            String strfasilitasoutdoor4 = a.getString(TAG_FASILITASOUTDOOR4);
                            String strfasilitasoutdoor5 = a.getString(TAG_FASILITASOUTDOOR5);
                            String strfasilitasoutdoor6 = a.getString(TAG_FASILITASOUTDOOR6);
                            String strfasilitasoutdoor7 = a.getString(TAG_FASILITASOUTDOOR7);
                            String strfasilitasoutdoor8 = a.getString(TAG_FASILITASOUTDOOR8);
                            String strfasilitasoutdoor9 = a.getString(TAG_FASILITASOUTDOOR9);
                            String strfasilitasoutdoor10 = a.getString(TAG_FASILITASOUTDOOR10);

                            if(strhargaoutdoor.equals("")) {
                                hargaoutdoor.setText("");
                            }
                            else{
                                hargaoutdoor.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strhargaoutdoor))).replace(",","."));
                            }

                            jumlahruanganoutdoor.setText(strjumlahruangoutdoor);
                            sisaruanganoutdoor.setText(strsisaruanganoutdoor);
                            fasilitasoutdoor1.setText(strfasilitasoutdoor1);
                            fasilitasoutdoor2.setText(strfasilitasoutdoor2);
                            fasilitasoutdoor3.setText(strfasilitasoutdoor3);
                            fasilitasoutdoor4.setText(strfasilitasoutdoor4);
                            fasilitasoutdoor5.setText(strfasilitasoutdoor5);
                            fasilitasoutdoor6.setText(strfasilitasoutdoor6);
                            fasilitasoutdoor7.setText(strfasilitasoutdoor7);
                            fasilitasoutdoor8.setText(strfasilitasoutdoor8);
                            fasilitasoutdoor9.setText(strfasilitasoutdoor9);
                            fasilitasoutdoor10.setText(strfasilitasoutdoor10);

                            if(!strgambaroutdoor.equals("")){
                                byte[] decodedString = Base64.decode(strgambaroutdoor, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                gambaroutdoor.setImageBitmap(decodedByte);
                            }

                        }

                    }
                    else{
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DetailIndoorOutdoorPengelola.this);
                        builder1.setMessage("Mohon maaf anda harus menambahkan jenis fasilitas pada lokasi "+getSharedPreferences("DATA",MODE_PRIVATE).getString("namalokasi",""));
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                    if(strgambarindoor.equals("")&&strgambaroutdoor.equals("")){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DetailIndoorOutdoorPengelola.this);
                        builder1.setMessage("Mohon maaf anda harus menambahkan jenis fasilitas pada lokasi "+getSharedPreferences("DATA",MODE_PRIVATE).getString("namalokasi",""));
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        pd.hide();
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                    else{
                        pd.hide();
                    }


                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");

            }
            pd.hide();
        }

    }
}
