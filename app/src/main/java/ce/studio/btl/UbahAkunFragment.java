package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;

public class UbahAkunFragment extends Fragment {

    Button btnsimpan,btnbatal;

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_ALAMAT = "alamat";
    private static final String TAG_TELP = "telp";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_PASSWORD = "password";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    EditText alamat,nomorhp,username,password,nama,email;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_ubah_akun, container, false);

        btnsimpan = (Button) rootview.findViewById(R.id.btnsimpan);
        btnbatal = (Button) rootview.findViewById(R.id.btnbatal);

        nama = (EditText) rootview.findViewById(R.id.nama);
        email = (EditText) rootview.findViewById(R.id.email);
        alamat = (EditText) rootview.findViewById(R.id.alamat);
        nomorhp = (EditText) rootview.findViewById(R.id.nomorhp);
        username = (EditText) rootview.findViewById(R.id.username);
        password = (EditText) rootview.findViewById(R.id.password);
        btnsimpan = (Button) rootview.findViewById(R.id.btnsimpan);

        new dataakun().execute();

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new simpanakun().execute();
            }
        });

        btnbatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.pertama,fragment);
                ft.commit();
            }
        });

        return rootview;
    }

    private class dataakun extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.DATAAKUN_URL;
            FEED_URL += "?iduser="+getActivity().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","");

            Log.e("Profile =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strnama = a.getString(TAG_NAMA);
                        String stremail = a.getString(TAG_EMAIL);
                        String stralamat = a.getString(TAG_ALAMAT);
                        String strtelp = a.getString(TAG_TELP);
                        String strusername = a.getString(TAG_USERNAME);
                        String strpassword = a.getString(TAG_PASSWORD);

                        nama.setText(strnama);
                        email.setText(stremail);
                        alamat.setText(stralamat);
                        nomorhp.setText(strtelp);
                        username.setText(strusername);
                        password.setText(strpassword);
                    }
                    pd.hide();
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }

    private class simpanakun extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {

            pd = new ProgressDialog(getContext());
            pd.setMessage("Sedang mengirim data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {
            return Simpan();
        }

        @SuppressWarnings("deprecation")
        private String Simpan() {
            String responseString = null;

            try {

                FEED_URL = Config.SIMPANAKUN_URL;
                FEED_URL += "?iduser="+getContext().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","");
                FEED_URL += "&" + URLEncoder.encode("nama", "UTF-8") + "=" + URLEncoder.encode(nama.getText().toString(), "UTF-8");
                FEED_URL += "&" + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email.getText().toString(), "UTF-8");
                FEED_URL += "&" + URLEncoder.encode("alamat", "UTF-8") + "=" + URLEncoder.encode(alamat.getText().toString(), "UTF-8");
                FEED_URL += "&" + URLEncoder.encode("nomorhp", "UTF-8") + "=" + URLEncoder.encode(nomorhp.getText().toString(), "UTF-8");
                FEED_URL += "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username.getText().toString(), "UTF-8");
                FEED_URL += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password.getText().toString(), "UTF-8");

                Log.e("Simpan =",FEED_URL);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(FEED_URL);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }


            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {

            if(result.equals("1")){
                Toast.makeText(getContext(), "Sukses mengirim komentar!", Toast.LENGTH_SHORT).show();
                Fragment fragment = new ProfileFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.pertama,fragment);
                ft.commit();
                pd.hide();
            }
            else{
                Toast.makeText(getContext(), "Gagal mengirim komentar!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }

            super.onPostExecute(result);
        }

    }
}