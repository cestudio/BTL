package ce.studio.btl;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * Created by ASUS on 2/5/2017.
 */
public class Komentar extends Dialog {
    public Activity c;
    public Button kirim;
    public EditText komentar;
    public ImageView like,unlike;

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    public Komentar(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layoutkomentar);
        like = (ImageView) findViewById(R.id.like);
        unlike = (ImageView) findViewById(R.id.unlike);
        komentar = (EditText) findViewById(R.id.komentar);
        kirim = (Button) findViewById(R.id.btnkirim);

        komentar.setVisibility(View.GONE);
        kirim.setVisibility(View.GONE);

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                like.setVisibility(View.GONE);
                unlike.setVisibility(View.GONE);
                komentar.setVisibility(View.VISIBLE);
                kirim.setVisibility(View.VISIBLE);

                SharedPreferences.Editor editor = getContext().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("statuskomentar", "1");
                editor.commit();
            }
        });

        unlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                like.setVisibility(View.GONE);
                unlike.setVisibility(View.GONE);
                komentar.setVisibility(View.VISIBLE);
                kirim.setVisibility(View.VISIBLE);

                SharedPreferences.Editor editor = getContext().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("statuskomentar", "0");
                editor.commit();
            }
        });

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new kirimkomentar().execute();
            }
        });


    }

    private class kirimkomentar extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {

            pd = new ProgressDialog(getContext());
            pd.setMessage("Sedang mengirim komentar, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            try {

                FEED_URL = Config.KIRIMKOMENTAR_URL;
                FEED_URL += "?idpemesanan="+getContext().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("idpemesanan","");
                FEED_URL += "&" + URLEncoder.encode("komentar", "UTF-8") + "=" + URLEncoder.encode(komentar.getText().toString(), "UTF-8");
                FEED_URL += "&statuskomentar="+getContext().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("statuskomentar","");

                Log.e("Komentar =",FEED_URL);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(FEED_URL);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }


            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {

            if(result.equals("1")){
                Toast.makeText(getContext(), "Sukses mengirim komentar!", Toast.LENGTH_SHORT).show();
                dismiss();
                ListPemesanan.fa.finish();

                pd.hide();
            }
            else if(result.equals("2")){
                Toast.makeText(getContext(), "Anda sudah mengirimkan komentar!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }
            else{
                Toast.makeText(getContext(), "Gagal mengirim komentar!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }

            super.onPostExecute(result);
        }

    }
}
