package ce.studio.btl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Line;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ce.studio.btl.Galeri.RequestHandler;

public class TambahLokasiPengelola extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public static Activity fa;

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    EditText namalokasi,alamatlokasi,kapasitaslokasi,deskripsilokasi;
    ImageView gambarlokasi,gambarindoor,gambaroutdoor;
    EditText jenislokasi;

    Button simpan,btnsetlokasi;

    static boolean a=false;

    TableLayout layoutnavigasi;
    LinearLayout layoutmap;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    private Uri fileUri1,fileUri2,fileUri3;

    public static final int MEDIA_TYPE_IMAGE = 1;

    private String filePath = null;

    private static final String TAG = TambahLokasiPengelola.class.getSimpleName();

    private int PICK_IMAGE_REQUEST = 1;

    private Bitmap bitmap1;

    String strimage1;

    String strjenislokasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_lokasi_pengelola);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa = this;

        layoutnavigasi = (TableLayout) findViewById(R.id.layoutnavigasi);
        layoutmap = (LinearLayout) findViewById(R.id.layoutmap);
        namalokasi = (EditText) findViewById(R.id.namalokasi);
        alamatlokasi = (EditText) findViewById(R.id.alamatlokasi);
        kapasitaslokasi = (EditText) findViewById(R.id.kapasitaslokasi);
        jenislokasi = (EditText) findViewById(R.id.jenislokasi);
        deskripsilokasi = (EditText) findViewById(R.id.deskripsilokasi);

        layoutnavigasi.setVisibility(View.GONE);
        layoutmap.setVisibility(View.GONE);

        gambarlokasi = (ImageView) findViewById(R.id.gambarlokasi);
//        btnsetlokasi = (Button) findViewById(R.id.btnsetlokasi);
        simpan = (Button) findViewById(R.id.btnsimpan);

        gambarlokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionGrantedGaleri1();
            }
        });

        SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
        editor.putString("next", "0");
        editor.commit();

        checkLocationPermission();

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(getSharedPreferences("DATA",MODE_PRIVATE).getString("next","").equals("0")){
//                    startActivity(new Intent(TambahLokasiPengelola.this,PencarianLokasi.class));
//                }
//                else{
                    uploadImageGaleri();
//                }

            }
        });

//        btnsetlokasi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(TambahLokasiPengelola.this,PencarianLokasi.class));
//            }
//        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public  boolean isStoragePermissionGrantedGaleri1() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                showFileChooser1();
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            showFileChooser1();
            return true;
        }
    }


    public void uploadImageGaleri(){

        final String stremailpemilik = getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("email","");
        final String strnamalokasi = namalokasi.getText().toString().trim();
        final String stralamatlokasi = alamatlokasi.getText().toString().trim();
        final String strkapasitaslokasi = kapasitaslokasi.getText().toString().trim();
        final String strjenislokasi = jenislokasi.getText().toString().trim();
        final String strdeskripsilokasi = deskripsilokasi.getText().toString().trim();
        final String strlatitude = getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("latitude","");
        final String strlongitude = getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("longitude","");
        if (fileUri1!=null){
            strimage1 = getStringImage(bitmap1);
        }

        class uploadImageGaleri extends AsyncTask<Void,Void,String>{
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = ProgressDialog.show(TambahLokasiPengelola.this,"Tunggu sebentar.....","Mengunggah",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pd.dismiss();
                if(!s.equals("111")){
                    Toast.makeText(TambahLokasiPengelola.this, "Gagal menambah lokasi!", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(TambahLokasiPengelola.this, "Sukses menambah lokasi!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> param = new HashMap<String,String>();
                param.put("emailpemilik",stremailpemilik);
                param.put("namalokasi",strnamalokasi);
                param.put("alamatlokasi",stralamatlokasi);
                param.put("kapasitaslokasi",strkapasitaslokasi);
                param.put("jenislokasi",strjenislokasi);
                param.put("deskripsilokasi",strdeskripsilokasi);
                param.put("latitude",strlatitude);
                param.put("longitude",strlongitude);

                if (strimage1!=null){
                    param.put("image1",strimage1);
                }

                String result = rh.sendPostRequest(Config.TAMBAHLOKASIPENGELOLA_URL, param);

                return result;
            }
        }
        uploadImageGaleri u = new uploadImageGaleri();
        u.execute();
    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                MarkerOptions markerOptions = new MarkerOptions();

                markerOptions.position(latLng);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("latitude", String.valueOf(latLng.latitude));
                editor.putString("longitude", String.valueOf(latLng.longitude));
                editor.commit();

                markerOptions.title("Lokasi Baru");

                mMap.clear();

                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                mMap.addMarker(markerOptions);
            }
        });

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, TambahLokasiPengelola.this);
        }

    }

    public void onConnectionSuspended(int i) {

    }

    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));


        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {


                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                }
                return;
            }

        }
    }

    public void onSearch(View view)
    {
        mMap.clear();
        String location = alamatlokasi.getText().toString();
        List<Address> addressList = null;
        if(location != null || !location.equals(""))
        {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location , 1);


            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addressList.size()>0){
                layoutnavigasi.setVisibility(View.VISIBLE);
                layoutmap.setVisibility(View.VISIBLE);
                Address address = addressList.get(0);
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("latitude", String.valueOf(address.getLatitude()));
                editor.putString("longitude", String.valueOf(address.getLongitude()));
                editor.commit();
                LatLng latLng = new LatLng(address.getLatitude() , address.getLongitude());
                mMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
            else{
                layoutnavigasi.setVisibility(View.GONE);
                layoutmap.setVisibility(View.GONE);
                Toast.makeText(TambahLokasiPengelola.this, "Lokasi tidak ditemukan", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void onZoom(View view)
    {
        if(view.getId() == R.id.Bzoomin)
        {
            mMap.animateCamera(CameraUpdateFactory.zoomIn());
        }
        if(view.getId() == R.id.Bzoomout)
        {
            mMap.animateCamera(CameraUpdateFactory.zoomOut());
        }
    }

    public void changeType(View view)
    {
        if(mMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL)
        {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }
        else
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    private void showFileChooser1() {
        Intent intent = new Intent();
        filePath = "1";
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Ambil Gambar"), PICK_IMAGE_REQUEST);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST){
            if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                if (filePath != null && filePath.equals("1")) {
                    previewMediaGaleri1(true,data);
                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void previewMediaGaleri1(boolean isImage, Intent data) {
        if (isImage) {
            fileUri1 = data.getData();
            try {
                bitmap1 = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri1);
                gambarlokasi.setImageBitmap(bitmap1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}