package ce.studio.btl;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DaftarKomentar extends AppCompatActivity {

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_KOMENTAR = "komentar";
    private static final String TAG_TANGGAL = "tanggal";
    private static final String TAG_JENISKOMENTAR = "jeniskomentar";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterListKomentar mGridAdapter;
    private ArrayList<GridItemListKomentar> mGridData;

    private static final String TAG = DaftarKomentar.class.getSimpleName();

    String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_komentar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mGridView = (GridView) findViewById(R.id.gridView);

        FEED_URL = Config.LISTKOMENTAR_URL;
        FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","");

        Log.e("Komentar =",FEED_URL);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterListKomentar(DaftarKomentar.this, R.layout.grid_item_layout_list_komentar, mGridData);
        mGridView.setAdapter(mGridAdapter);

        pd = new ProgressDialog(DaftarKomentar.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        new getKomentar().execute(FEED_URL);
    }

    public class getKomentar extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    response = streamToString(httpResponse.getEntity().getContent());

                    parseResult(response);

                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray(TAG_RESULT);
            GridItemListKomentar item;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String strnama = post.getString(TAG_NAMA);
                String stremail = post.getString(TAG_EMAIL);
                String strkomentar = post.getString(TAG_KOMENTAR);
                String strtanggal = post.getString(TAG_TANGGAL);
                String strjeniskomentar = post.getString(TAG_JENISKOMENTAR);
                item = new GridItemListKomentar();
                item.setNama(strnama);
                item.setEmail(stremail);
                item.setKomentar(strkomentar);
                item.setTanggal(strtanggal);
                item.setJeniskomentar(strjeniskomentar);

                mGridData.add(item);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
