package ce.studio.btl;

public class GridItemListPemesanan {

    public String getJenislokasi() {
        return jenislokasi;
    }

    public void setJenislokasi(String jenislokasi) {
        this.jenislokasi = jenislokasi;
    }

    String jenislokasi,namapemilik,jumlahharga,namalokasi,tipelokasi,hargalokasi,nomorpemesanan,lamabooking,jumlahruangan,diskon,statuspemesanan,validasi,gambar;

    public String getNamapemilik() {

        return namapemilik;

    }

    public String getValidasi() {
        return validasi;
    }

    public void setValidasi(String validasi) {
        this.validasi = validasi;
    }

    public void setNamapemilik(String namapemilik) {

        this.namapemilik = namapemilik;

    }

    public String getJumlahharga() {

        return jumlahharga;

    }

    public void setJumlahharga(String jumlahharga) {

        this.jumlahharga = jumlahharga;

    }

    public String getNamalokasi() {

        return namalokasi;

    }

    public void setNamalokasi(String namalokasi) {

        this.namalokasi = namalokasi;

    }

    public String getTipelokasi() {

        return tipelokasi;

    }

    public void setTipelokasi(String tipelokasi) {

        this.tipelokasi = tipelokasi;

    }

    public String getNomorpemesanan() {

        return nomorpemesanan;

    }

    public void setNomorpemesanan(String nomorpemesanan) {

        this.nomorpemesanan = nomorpemesanan;

    }

    public String getLamabooking() {

        return lamabooking;

    }

    public void setLamabooking(String lamabooking) {

        this.lamabooking = lamabooking;

    }

    public String getJumlahruangan() {

        return jumlahruangan;

    }

    public void setJumlahruangan(String jumlahruangan) {

        this.jumlahruangan = jumlahruangan;

    }

    public String getDiskon() {

        return diskon;

    }

    public void setDiskon(String diskon) {

        this.diskon = diskon;

    }

    public String getStatuspemesanan() {

        return statuspemesanan;

    }

    public void setStatuspemesanan(String statuspemesanan) {

        this.statuspemesanan = statuspemesanan;

    }

    public String getHargalokasi() {

        return hargalokasi;

    }

    public void setHargalokasi(String hargalokasi) {

        this.hargalokasi = hargalokasi;

    }

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }
}