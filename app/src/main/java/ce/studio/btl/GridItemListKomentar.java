package ce.studio.btl;

public class GridItemListKomentar {

    String nama,email,komentar,tanggal,jeniskomentar;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJeniskomentar() {
        return jeniskomentar;
    }

    public void setJeniskomentar(String jeniskomentar) {
        this.jeniskomentar = jeniskomentar;
    }
}