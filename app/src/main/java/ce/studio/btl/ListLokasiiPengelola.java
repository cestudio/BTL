package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ListLokasiiPengelola extends AppCompatActivity {

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDLOKASI = "idlokasi";
    private static final String TAG_NAMALOKASI = "namalokasi";
    private static final String TAG_GAMBAR = "gambar";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterListLokasi mGridAdapter;
    private ArrayList<GridItemLisLokasi> mGridData;

    private static final String TAG = ListLokasiiPengelola.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_lokasi_pengelola);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mGridView = (GridView) findViewById(R.id.gridView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        FEED_URL = Config.MAPLOKASIPENGGUNA_URL;
        FEED_URL += "?email="+getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("email","");

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterListLokasi(ListLokasiiPengelola.this, R.layout.grid_item_layout_list_lokasi, mGridData);
        mGridView.setAdapter(mGridAdapter);

        pd = new ProgressDialog(ListLokasiiPengelola.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        //Start download
        new getLokasi().execute(FEED_URL);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ListLokasiiPengelola.this,TambahLokasiPengelola.class));
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemLisLokasi item = (GridItemLisLokasi) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idlokasi", item.getIdlokasi());
                editor.putString("namalokasi", item.getNamalokasi());
                editor.commit();

                //Pass the image title and url to DetailsActivity
                if(getIntent().getStringExtra("jenislist").equals("lokasi")){
                    Intent intent = new Intent(ListLokasiiPengelola.this, DetailLokasiPengelola.class);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(ListLokasiiPengelola.this, TambahIndoorOutdoorPengelola.class);
                    startActivity(intent);
                }

            }
        });
    }

    //Downloading data asynchronously
    public class getLokasi extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray(TAG_RESULT);
            GridItemLisLokasi item;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String stridlokasi = post.getString(TAG_IDLOKASI);
                String strnamalokasi = post.getString(TAG_NAMALOKASI);
                item = new GridItemLisLokasi();
                item.setIdlokasi(stridlokasi);
                item.setNamalokasi(strnamalokasi);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString(TAG_GAMBAR));
                }

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
