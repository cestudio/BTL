package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.components.Legend;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class HalamanUtamaPengelola extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG_RESULT = "result";
    private static final String TAG_INDOOR = "indoor";
    private static final String TAG_OUTDOOR = "outdoor";
    private static final String TAG_IDLOKASI = "idlokasi";
    private static final String TAG_NAMALOKASI = "namalokasi";
    private static final String TAG_GAMBAR = "gambar";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterListLokasi mGridAdapter;
    private ArrayList<GridItemLisLokasi> mGridData;

    private static final String TAG = HalamanUtama.class.getSimpleName();

    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_utama_pengelola);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header= navigationView.getHeaderView(0);
        TextView nama=(TextView) header.findViewById(R.id.nama);
        TextView email=(TextView) header.findViewById(R.id.email);
        nama.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("nama", ""));
        email.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("email", ""));

        mGridView = (GridView) findViewById(R.id.gridView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HalamanUtamaPengelola.this,TambahLokasiPengelola.class));
            }
        });

        FEED_URL = Config.MAPLOKASIPENGGUNA_URL;//iki  lo pak
        FEED_URL += "?email="+getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("email","");

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterListLokasi(HalamanUtamaPengelola.this, R.layout.grid_item_layout_list_lokasi, mGridData);
        mGridView.setAdapter(mGridAdapter);

        pd = new ProgressDialog(HalamanUtamaPengelola.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        //Start download
        new getLokasi().execute(FEED_URL);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FEED_URL = Config.MAPLOKASIPENGGUNA_URL;
                FEED_URL += "?email="+getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("email","");

                //Initialize with empty data
                mGridData = new ArrayList<>();
                mGridAdapter = new GridViewAdapterListLokasi(HalamanUtamaPengelola.this, R.layout.grid_item_layout_list_lokasi, mGridData);
                mGridView.setAdapter(mGridAdapter);

                pd = new ProgressDialog(HalamanUtamaPengelola.this);
                pd.setMessage("Sedang mengambil data, tunggu sebentar...");
                pd.setCancelable(false);
                pd.show();

                //Start download
                new getLokasi().execute(FEED_URL);

                swipeRefreshLayout.setRefreshing(false);
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemLisLokasi item = (GridItemLisLokasi) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idlokasi", item.getIdlokasi());
                editor.putString("namalokasi", item.getNamalokasi());
                editor.commit();

                startDialog();


            }
        });
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        swipeRefreshLayout.setRefreshing(false);
    }

    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(HalamanUtamaPengelola.this);
        myAlertDialog.setTitle("Opsi Lokasi");

        myAlertDialog.setPositiveButton("Detail Lokasi",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(HalamanUtamaPengelola.this, DetailLokasiPengelola.class);
                        startActivity(intent);
                    }
                });

        myAlertDialog.setNegativeButton("Lihat Fasilitas Indoor Outdoor",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        startActivity(new Intent(HalamanUtamaPengelola.this,DetailIndoorOutdoorPengelola.class));
                    }
                });

        myAlertDialog.setNeutralButton("Tambah Fasilitas Indoor Outdoor",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(HalamanUtamaPengelola.this, TambahIndoorOutdoorPengelola.class);
                        startActivity(intent);
                    }
                });
        myAlertDialog.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_kelola_akun) {
            startActivity(new Intent(HalamanUtamaPengelola.this,KelolaAkun.class));
        }
        else if (id == R.id.nav_lihat_laporan_lokasi) {
            startActivity(new Intent(HalamanUtamaPengelola.this,ListLaporanPengelola.class));
        } else if (id == R.id.nav_lihat_jadwal) {
            startActivity(new Intent(HalamanUtamaPengelola.this,ListJadwaliPengelola.class));
        } else if (id == R.id.nav_lihat_komentar) {
            startActivity(new Intent(HalamanUtamaPengelola.this,ListLokasiKomentar.class));
        } else if (id == R.id.nav_keluar) {
            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
            editor.clear();
            editor.commit();

            startActivity(new Intent(HalamanUtamaPengelola.this,Login.class));
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Downloading data asynchronously
    public class getLokasi extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray(TAG_RESULT);
            GridItemLisLokasi item;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String stridlokasi = post.getString(TAG_IDLOKASI);
                String strnamalokasi = post.getString(TAG_NAMALOKASI);
                item = new GridItemLisLokasi();
                item.setIdlokasi(stridlokasi);
                item.setNamalokasi(strnamalokasi);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString(TAG_GAMBAR));
                }

                mGridData.add(item);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
