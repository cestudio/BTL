package ce.studio.btl;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Pemesanan extends AppCompatActivity {

    public GregorianCalendar cal_month, cal_month_copy;
    private CalendarAdapter cal_adapter;
    private TextView tv_month;

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_TANGGAL = "tanggal";
    private static final String TAG_LAMASEWAJAM = "lamasewajam";
    private static final String TAG_GAMBAR = "gambar";
    private static final String TAG_BUATPESANAN = "buatpesanan";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    GridView gridview;

    EditText tanggalpesan,waktupesan,jumlahruangan,lamajam;

    TextView buatpesanan;

    ProgressDialog pd;

    private DatePickerDialog tanggal;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat timeFormatter;

    java.sql.Time timeValue;

    ImageView gambarfasilitas;

    private int myear;
    private int mmonth;
    private int mday;

    static final int DATE_DIALOG_ID = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tanggalpesan = (EditText) findViewById(R.id.tanggalpesan);
        waktupesan = (EditText) findViewById(R.id.waktupesan);
        jumlahruangan = (EditText) findViewById(R.id.jumlahruangan);
        lamajam = (EditText) findViewById(R.id.lamajam);
        buatpesanan = (TextView) findViewById(R.id.buatpesanan);
        gambarfasilitas = (ImageView) findViewById(R.id.gambar);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        new setGambar().execute();

        final Calendar c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        tanggalpesan.setText(new StringBuilder().append(myear)
                .append("-").append(mmonth + 1).append("-").append(mday));

        tanggalpesan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);
            }
        });

        waktupesan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Calendar  c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int min = c.get(Calendar.MINUTE);

                TimePickerDialog td = new TimePickerDialog(Pemesanan.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                try {
                                    String dtStart = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                                    timeFormatter = new SimpleDateFormat("HH:mm");

                                    timeValue = new java.sql.Time(timeFormatter.parse(dtStart).getTime());
                                    waktupesan.setText(String.valueOf(timeValue));
                                } catch (Exception ex) {
                                    waktupesan.setText(ex.getMessage().toString());
                                }
                            }
                        },
                        hour, min,
                        DateFormat.is24HourFormat(Pemesanan.this)
                );
                td.show();

            }
        });



        buatpesanan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tanggalpesan.getText().toString().equals("")){
                    Toast.makeText(Pemesanan.this, "Tanggal awal pemesanan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                }
                else if(waktupesan.getText().toString().equals("")){
                    Toast.makeText(Pemesanan.this, "Tanggal akhir pemesanan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                }
                else if(jumlahruangan.getText().toString().equals("")){
                    Toast.makeText(Pemesanan.this, "Jumlah ruangan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                }
                else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(Pemesanan.this);
                    builder1.setMessage("Apa data yang anda isi benar?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    new buatpesanan().execute();
                                }
                            });

                    builder1.setNegativeButton(
                            "Tidak",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });
    }


    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                DatePickerDialog _date =   new DatePickerDialog(this, datePickerListener, myear,mmonth,
                        mday){
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        if (year < myear) {
                            view.updateDate(myear, mmonth, mday);
                            Toast.makeText(Pemesanan.this, "Tanggal Kadaluarsa", Toast.LENGTH_SHORT).show();
                        }

                        if (monthOfYear < mmonth && year == myear) {
                            view.updateDate(myear, mmonth, mday);
                            Toast.makeText(Pemesanan.this, "Tanggal Kadaluarsa", Toast.LENGTH_SHORT).show();
                        }

                        if (dayOfMonth < mday && year == myear && monthOfYear == mmonth) {
                            view.updateDate(myear, mmonth, mday);
                            Toast.makeText(Pemesanan.this, "Tanggal Kadaluarsa", Toast.LENGTH_SHORT).show();
                        }

                    }
                };

                return _date;
        }

        return null;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            myear = selectedYear;
            mmonth = selectedMonth;
            mday = selectedDay;

            // set selected date into textview
            tanggalpesan.setText(new StringBuilder().append(myear)
                    .append("-").append(mmonth + 1).append("-").append(mday));

        }
    };

    private class setGambar extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(Pemesanan.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.GALERY_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");
            FEED_URL += "&kategori="+getSharedPreferences("DATA",MODE_PRIVATE).getString("kategori","").replace(" ","%20");

            Log.e("Gambar =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String gambar = a.getString(TAG_GAMBAR);

                            byte[] decodedString = Base64.decode(gambar, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            gambarfasilitas.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, 500, 300, false));
                        }
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");

            }
            pd.hide();
        }

    }

    private class buatpesanan extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(Pemesanan.this);
            pd.setMessage("Membuat pesanan, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.BUATPESANAN_URL;
            FEED_URL += "?idpelanggan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");
            FEED_URL += "&idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");
            FEED_URL += "&kategori="+getSharedPreferences("DATA",MODE_PRIVATE).getString("kategori","");
            FEED_URL += "&tanggalpesan="+tanggalpesan.getText().toString().replace(" ","%20");
            FEED_URL += "&waktupesan="+waktupesan.getText().toString().replace(" ","%20");
            FEED_URL += "&jumlahruangan="+jumlahruangan.getText().toString().replace(" ","%20");
            FEED_URL += "&lamajam="+lamajam.getText().toString().replace(" ","%20");
            FEED_URL += "&token="+getSharedPreferences("DATA",MODE_PRIVATE).getString("token","");

            Log.e("Pemesanan = ",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String buatpesan = a.getString(TAG_BUATPESANAN);

                        if (buatpesan.equals("1")) {
                            Toast.makeText(Pemesanan.this, "Berhasil membuat pemesanan!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Pemesanan.this,ListPemesanan.class));
                            finish();
                        }
                        else if (buatpesan.equals("2")) {
                            Toast.makeText(Pemesanan.this, "Jam tersebut telah dibooking!", Toast.LENGTH_SHORT).show();
                        }
                        else if (buatpesan.equals("3")) {
                            Toast.makeText(Pemesanan.this, "Jumlah ruangan yang dipesan melebihi sisa ruangan!", Toast.LENGTH_SHORT).show();
                        }
                        else if (buatpesan.equals("4")) {
                            Toast.makeText(Pemesanan.this, "Lama sewa minimal 3 jam!", Toast.LENGTH_SHORT).show();
                        }
                        else if (buatpesan.equals("5")) {
                            Toast.makeText(Pemesanan.this, "Token tidak valid!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(Pemesanan.this, "Gagal membuat pemesanan!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    pd.hide();
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }


}
