package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;


public class GambarFasilitas extends AppCompatActivity {

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_GAMBAR = "gambar";
    private static final String TAG_FASILITAS1 = "fasilitas1";
    private static final String TAG_FASILITAS2 = "fasilitas2";
    private static final String TAG_FASILITAS3 = "fasilitas3";
    private static final String TAG_FASILITAS4 = "fasilitas4";
    private static final String TAG_FASILITAS5 = "fasilitas5";
    private static final String TAG_FASILITAS6 = "fasilitas6";
    private static final String TAG_FASILITAS7 = "fasilitas7";
    private static final String TAG_FASILITAS8 = "fasilitas8";
    private static final String TAG_FASILITAS9 = "fasilitas9";
    private static final String TAG_FASILITAS10 = "fasilitas10";
    private static final String TAG_DESKRIPSI = "deskripsi";
    private static final String TAG_HARGA = "harga";
    private static final String TAG_SISA = "sisa";
    private static final String TAG_ALAMAT = "alamat";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    Button btnbooknow;

    TextView datafasilitas,datadeskripsi,dataharga,datasisa,dataalamat;

    ProgressDialog pd;

    ImageView gambarfasilitas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_indoor_outdoor_pengelola);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnbooknow = (Button) findViewById(R.id.btnbooknow);
        datafasilitas = (TextView) findViewById(R.id.datafasilitas);
        datadeskripsi = (TextView) findViewById(R.id.datadeskripsi);
        dataharga = (TextView) findViewById(R.id.dataharga);
        datasisa = (TextView) findViewById(R.id.datasisa);
        dataalamat = (TextView) findViewById(R.id.dataalamat);
        gambarfasilitas = (ImageView) findViewById(R.id.gambar);

        new setGambar().execute();
        new setDetail().execute();

        btnbooknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GambarFasilitas.this,Pemesanan.class));
                finish();
            }
        });

    }

    private class setGambar extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(GambarFasilitas.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.GALERY_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");
            FEED_URL += "&kategori="+getSharedPreferences("DATA",MODE_PRIVATE).getString("kategori","").replace(" ","%20");

            Log.e("Gambar =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String gambar = a.getString(TAG_GAMBAR);

                            byte[] decodedString = Base64.decode(gambar, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            gambarfasilitas.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, 500, 300, false));
                        }
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");

            }
        }

    }

    private class setDetail extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.DETAILMAPINDOOROUTDOOR_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");
            FEED_URL += "&kategori="+getSharedPreferences("DATA",MODE_PRIVATE).getString("kategori","").replace(" ","%20");

            Log.e("Detail =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String fasilitas1 = a.getString(TAG_FASILITAS1);
                            String fasilitas2 = a.getString(TAG_FASILITAS2);
                            String fasilitas3 = a.getString(TAG_FASILITAS3);
                            String fasilitas4 = a.getString(TAG_FASILITAS4);
                            String fasilitas5 = a.getString(TAG_FASILITAS5);
                            String fasilitas6 = a.getString(TAG_FASILITAS6);
                            String fasilitas7 = a.getString(TAG_FASILITAS7);
                            String fasilitas8 = a.getString(TAG_FASILITAS8);
                            String fasilitas9 = a.getString(TAG_FASILITAS9);
                            String fasilitas10 = a.getString(TAG_FASILITAS10);
                            String deskripsi = a.getString(TAG_DESKRIPSI);
                            String harga = a.getString(TAG_HARGA);
                            String sisa = a.getString(TAG_SISA);
                            String alamat = a.getString(TAG_ALAMAT);

                            String fasilitas="";

                            if(!fasilitas1.equals("")){
                                fasilitas += " - "+fasilitas1;
                            }
                            if(!fasilitas2.equals("")){
                                fasilitas += "\n - "+fasilitas2;
                            }
                            if(!fasilitas3.equals("")){
                                fasilitas += "\n - "+fasilitas3;
                            }
                            if(!fasilitas4.equals("")){
                                fasilitas += "\n - "+fasilitas4;
                            }
                            if(!fasilitas5.equals("")){
                                fasilitas += "\n - "+fasilitas5;
                            }
                            if(!fasilitas6.equals("")){
                                fasilitas += "\n - "+fasilitas6;
                            }
                            if(!fasilitas7.equals("")){
                                fasilitas += "\n - "+fasilitas7;
                            }
                            if(!fasilitas8.equals("")){
                                fasilitas += "\n - "+fasilitas8;
                            }
                            if(!fasilitas9.equals("")){
                                fasilitas += "\n - "+fasilitas9;
                            }
                            if(!fasilitas10.equals("")){
                                fasilitas += "\n - "+fasilitas10;
                            }

                            datafasilitas.setText(fasilitas);
                            datadeskripsi.setText(deskripsi);
                            dataharga.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(harga))).replace(",",".")+" / 3 Jam");
                            datasisa.setText(sisa);
                            dataalamat.setText(alamat);

                        }
                    }
                    else{
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(GambarFasilitas.this);
                        builder1.setMessage("Mohon maaf detail fasilitas untuk "+getSharedPreferences("DATA",MODE_PRIVATE).getString("namalokasi","")+" tidak diketahui");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");

            }
            pd.hide();
        }

    }
}