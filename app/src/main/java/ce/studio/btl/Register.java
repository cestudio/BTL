package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Register extends AppCompatActivity {

    Button btnregister;

    EditText username,password,nama,email,telp,alamat;

    private static final String TAG_RESULT = "result";
    private static final String TAG_REGISTER = "register";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    Spinner jenisuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        nama = (EditText) findViewById(R.id.nama);
        email = (EditText) findViewById(R.id.email);
        telp = (EditText) findViewById(R.id.telp);
        alamat = (EditText) findViewById(R.id.alamat);
        btnregister = (Button) findViewById(R.id.btnregister);
        jenisuser = (Spinner) findViewById(R.id.jenisuser);

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(jenisuser.getSelectedItemPosition()==0){
                    Toast.makeText(Register.this, "Pilih jenis user", Toast.LENGTH_SHORT).show();
                }
                else{
                    pd = new ProgressDialog(Register.this);
                    pd.setMessage("Sedang mengambil data, tunggu sebentar...");
                    pd.setCancelable(false);
                    pd.show();
                    new register().execute();
                }
            }
        });


    }

    private class register extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(Register.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {

//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.REGISTER_URL;
            FEED_URL += "?username="+username.getText().toString().replace(" ","%20");
            FEED_URL += "&password="+password.getText().toString().replace(" ","%20");
            FEED_URL += "&nama="+nama.getText().toString().replace(" ","%20");
            FEED_URL += "&email="+email.getText().toString().replace(" ","%20");
            FEED_URL += "&telp="+telp.getText().toString().replace(" ","%20");
            FEED_URL += "&alamat="+alamat.getText().toString().replace(" ","%20");
            FEED_URL += "&jenisuser="+jenisuser.getSelectedItem().toString().replace(" ","%20");

            Log.e("Pendaftaran =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String register = a.getString(TAG_REGISTER);

                        if(register.equals("1")){
                            Toast.makeText(Register.this, "Pendaftaran berhasil!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else if(register.equals("2")){
                            Toast.makeText(Register.this, "Username/password sudah digunakan!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(Register.this, "Pendaftaran tidak berhasil!", Toast.LENGTH_SHORT).show();
                        }

                    }
                    pd.hide();
                }
                catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }
            pd.hide();
        }
    }
}
