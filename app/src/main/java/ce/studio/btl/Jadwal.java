package ce.studio.btl;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class Jadwal extends AppCompatActivity implements OnClickListener {

    private ListView lv_android;
    private AndroidListAdapter list_adapter;
    private Button btn_calender;

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_TANGGAL = "tanggal";
    private static final String TAG_TANGGALAKHIR = "tanggalakhir";
    private static final String TAG_LAMASEWAJAM = "lamasewajam";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new setKalender().execute();
    }

    public void getWidget(){

        lv_android = (ListView) findViewById(R.id.lv_android);
        list_adapter=new AndroidListAdapter(Jadwal.this,R.layout.list_item, CalendarCollection.date_collection_arr);
        lv_android.setAdapter(list_adapter);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
//            case R.id.btn_calender:
//                startActivity(new Intent(Jadwal.this,CalenderActivity.class));
//
//                break;

            default:
                break;
        }

    }

    private class setKalender extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(Jadwal.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.KALENDER_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    CalendarCollection.date_collection_arr=new ArrayList<CalendarCollection>();

                    if(rs.length()==0){
                        Toast.makeText(Jadwal.this, "Tidak ada jadwal booking pada "+getSharedPreferences("DATA",MODE_PRIVATE).getString("namalokasi",""), Toast.LENGTH_SHORT).show();
                        pd.hide();
                    }
                    else{
                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String nama = "Nama Pemesan : "+a.getString(TAG_NAMA);
                            String tanggal = "Tanggal Pesan : "+a.getString(TAG_TANGGAL);
                            String tanggalselesai = "Tanggal Selesai : "+a.getString(TAG_TANGGALAKHIR);
                            String lamasewajam = a.getString(TAG_LAMASEWAJAM);

                            CalendarCollection.date_collection_arr.add(new CalendarCollection(tanggal+"\n"+tanggalselesai,nama+"\nLama Sewa : "+lamasewajam+" Jam"));

                        }
                        getWidget();
                        pd.hide();
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}
