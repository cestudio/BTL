package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ListLaporanPengelola extends AppCompatActivity {

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDLOKASI = "idlokasi";
    private static final String TAG_NAMALOKASI = "namalokasi";
    private static final String TAG_GAMBAR = "gambar";
    private static final String TAG_INDOOR = "indoor";
    private static final String TAG_OUTDOOR = "outdoor";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    ImageView imgindoor,imgoutdoor;
    TextView indoorbook,outdoorbook;
    TextView indoortext,outdoortext,tutup;

    ScrollView layoutindooroutdoor;

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterListLokasi mGridAdapter;
    private ArrayList<GridItemLisLokasi> mGridData;

    private static final String TAG = ListLaporanPengelola.class.getSimpleName();

    EditText carilokasi;

    Button btncari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_lokasi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mGridView = (GridView) findViewById(R.id.gridView);

        tutup = (TextView) findViewById(R.id.tutup);
        indoorbook = (TextView) findViewById(R.id.indoorbook);
        indoortext = (TextView) findViewById(R.id.indoortext);
        outdoorbook = (TextView) findViewById(R.id.outdoorbook);
        outdoortext = (TextView) findViewById(R.id.outdoortext);
        imgindoor = (ImageView) findViewById(R.id.imgindoor);
        imgoutdoor = (ImageView) findViewById(R.id.imgoutdoor);
        layoutindooroutdoor = (ScrollView) findViewById(R.id.layoutindooroutdoor);
        carilokasi = (EditText) findViewById(R.id.carilokasi);
        btncari = (Button) findViewById(R.id.btncari);

        FEED_URL = Config.MAPLOKASIPENGGUNA_URL;
        FEED_URL += "?email="+getSharedPreferences("DATA",MODE_PRIVATE).getString("email","");

        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterListLokasi(ListLaporanPengelola.this, R.layout.grid_item_layout_list_lokasi, mGridData);
        mGridView.setAdapter(mGridAdapter);

        pd = new ProgressDialog(ListLaporanPengelola.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        layoutindooroutdoor.setVisibility(View.GONE);

        new getLokasi().execute(FEED_URL);

        tutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutindooroutdoor.setVisibility(View.GONE);
            }
        });

        imgindoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("kategori", "indoor");
                editor.commit();

                startActivity(new Intent(ListLaporanPengelola.this,GrafikLaporanPengelola.class));
            }
        });

        imgoutdoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("kategori", "outdoor");
                editor.commit();

                startActivity(new Intent(ListLaporanPengelola.this,GrafikLaporanPengelola.class));
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemLisLokasi item = (GridItemLisLokasi) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idlokasi", item.getIdlokasi());
                editor.putString("namalokasi", item.getNamalokasi());
                editor.commit();

                new setIndoorOutdoor().execute();
            }
        });

        btncari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FEED_URL = Config.CARIMAP_URL;
                FEED_URL += "?namalokasi="+carilokasi.getText().toString().replace(" ","%20");
                //Initialize with empty data
                mGridData = new ArrayList<>();
                mGridAdapter = new GridViewAdapterListLokasi(ListLaporanPengelola.this, R.layout.grid_item_layout_list_lokasi, mGridData);
                mGridView.setAdapter(mGridAdapter);

                pd = new ProgressDialog(ListLaporanPengelola.this);
                pd.setMessage("Sedang mengambil data, tunggu sebentar...");
                pd.setCancelable(false);
                pd.show();

                layoutindooroutdoor.setVisibility(View.GONE);
                //Start download
                new getLokasi().execute(FEED_URL);
            }
        });
    }

    //Downloading data asynchronously
    public class getLokasi extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemLisLokasi item;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String stridlokasi = post.getString(TAG_IDLOKASI);
                String strnamalokasi = post.getString(TAG_NAMALOKASI);
                item = new GridItemLisLokasi();
                item.setIdlokasi(stridlokasi);
                item.setNamalokasi(strnamalokasi);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString(TAG_GAMBAR));
                }

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class setIndoorOutdoor extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(ListLaporanPengelola.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.MAPINDOOROUTDOOR_URL;
            FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","").replace(" ","%20");;

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        for(int i=0; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String indoor = a.getString(TAG_INDOOR);
                            String outdoor = a.getString(TAG_OUTDOOR);

                            if(!indoor.equals("")){
                                byte[] decodedString1 = Base64.decode(indoor, Base64.DEFAULT);
                                Bitmap decodedByte1 = BitmapFactory.decodeByteArray(decodedString1, 0, decodedString1.length);
                                imgindoor.setImageBitmap(Bitmap.createScaledBitmap(decodedByte1, 150, 150, false));
                                indoortext.setVisibility(View.VISIBLE);
                                imgindoor.setVisibility(View.VISIBLE);
                                indoorbook.setVisibility(View.VISIBLE);
                            }
                            else{
                                indoortext.setVisibility(View.INVISIBLE);
                                imgindoor.setVisibility(View.INVISIBLE);
                                indoorbook.setVisibility(View.INVISIBLE);
                            }

                            if(!outdoor.equals("")){
                                byte[] decodedString2 = Base64.decode(outdoor, Base64.DEFAULT);
                                Bitmap decodedByte2 = BitmapFactory.decodeByteArray(decodedString2, 0, decodedString2.length);
                                imgoutdoor.setImageBitmap(Bitmap.createScaledBitmap(decodedByte2, 150, 150, false));
                                outdoortext.setVisibility(View.VISIBLE);
                                imgoutdoor.setVisibility(View.VISIBLE);
                                outdoorbook.setVisibility(View.VISIBLE);
                            }
                            else{
                                outdoortext.setVisibility(View.INVISIBLE);
                                imgoutdoor.setVisibility(View.INVISIBLE);
                                outdoorbook.setVisibility(View.INVISIBLE);
                            }

                            if (indoor.equals("")&&outdoor.equals("")){
                                layoutindooroutdoor.setVisibility(View.GONE);
                                Toast.makeText(ListLaporanPengelola.this, "Anda belum menambahkan fasilitas", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                layoutindooroutdoor.setVisibility(View.VISIBLE);
                            }
                        }
                        layoutindooroutdoor.setVisibility(View.VISIBLE);
                    }
                    else{
                        layoutindooroutdoor.setVisibility(View.GONE);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ListLaporanPengelola.this);
                        builder1.setMessage("Maaf pengelola belum menambahkan fasilitas lokasi");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                    pd.hide();
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
            pd.hide();
        }

    }
}
