package ce.studio.btl;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.HashMap;

public class CaraPemesanan extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    SliderLayout sliderLayout;
    HashMap<String,String> Hash_file_maps ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cara_pemesanan);

        Hash_file_maps = new HashMap<String, String>();

        sliderLayout = (SliderLayout)findViewById(R.id.slider);

        Hash_file_maps.put("Pilih lokasi yang ingin dibooking.\n\n", "http://192.168.23.1/btl/gambar1.png");
        Hash_file_maps.put("Pilih jenis lokasi indoor atau outdoor.\n\n", "http://192.168.23.1/btl/gambar2.png");
        Hash_file_maps.put("Klik Book Now untuk melakukan pemesanan.\n\n", "http://192.168.23.1/btl/gambar3.png");
        Hash_file_maps.put("Cek jadwal yang belum di booking.\n\n", "http://192.168.23.1/btl/gambar4.png");
        Hash_file_maps.put("Masukkan tanggal dan jumlah ruangan yang dibooking.\n\n", "http://192.168.23.1/btl/gambar5.png");
        Hash_file_maps.put("Jika berhasil melakukan pemesanan, akan tampil pada menu list reservasi.\n\n", "http://192.168.23.1/btl/gambar6.png");
        Hash_file_maps.put("Detail reservasi dari hasil proses booking lokasi.\n\n", "http://192.168.23.1/btl/gambar7.png");

        for(String name : Hash_file_maps.keySet()){

            TextSliderView textSliderView = new TextSliderView(CaraPemesanan.this);
            textSliderView
                    .description(name)
                    .image(Hash_file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setDuration(5000);
        sliderLayout.addOnPageChangeListener(this);
    }
    @Override
    protected void onStop() {

        sliderLayout.stopAutoCycle();

        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

//        Toast.makeText(this,slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {

        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}
}