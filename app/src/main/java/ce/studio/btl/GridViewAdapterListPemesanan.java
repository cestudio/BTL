package ce.studio.btl;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterListPemesanan extends ArrayAdapter<GridItemListPemesanan> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemListPemesanan> mGridData = new ArrayList<GridItemListPemesanan>();

    public GridViewAdapterListPemesanan(Context mContext, int layoutResourceId, ArrayList<GridItemListPemesanan> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemListPemesanan> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.namapemilik = (TextView) row.findViewById(R.id.namapemilik);
            holder.jumlahharga = (TextView) row.findViewById(R.id.jumlahharga);
            holder.namalokasi = (TextView) row.findViewById(R.id.namalokasi);
            holder.tipelokasi = (TextView) row.findViewById(R.id.tipelokasi);
            holder.jenislokasi = (TextView) row.findViewById(R.id.jenislokasi);
            holder.hargalokasi = (TextView) row.findViewById(R.id.hargalokasi);
            holder.nomorpemesanan = (TextView) row.findViewById(R.id.nomorpemesanan);
            holder.lamabooking = (TextView) row.findViewById(R.id.lamabooking);
            holder.jumlahruangan = (TextView) row.findViewById(R.id.jumlahruangan);
            holder.validasi = (TextView) row.findViewById(R.id.nbvalidasi);
            holder.statuspemesanan = (TextView) row.findViewById(R.id.statuspemesanan);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemListPemesanan item = mGridData.get(position);
        holder.namapemilik.setText(Html.fromHtml(item.getNamapemilik()));
        holder.jumlahharga.setText(Html.fromHtml(item.getJumlahharga()));
        holder.namalokasi.setText(Html.fromHtml(item.getNamalokasi()));
        holder.tipelokasi.setText(Html.fromHtml(item.getTipelokasi()));
        holder.jenislokasi.setText(Html.fromHtml(item.getJenislokasi()));
        holder.hargalokasi.setText(Html.fromHtml(item.getHargalokasi()));
        holder.nomorpemesanan.setText(Html.fromHtml(item.getNomorpemesanan()));
        holder.lamabooking.setText(Html.fromHtml(item.getLamabooking())+ " jam");
        holder.jumlahruangan.setText(Html.fromHtml(item.getJumlahruangan()));
        holder.validasi.setText(Html.fromHtml(item.getValidasi()));
        holder.statuspemesanan.setText(Html.fromHtml(item.getStatuspemesanan()));

        byte[] decodedString = Base64.decode(item.getGambar(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.gambar.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, 300, 300, false));

//        if(item.getStatuspemesanan().equals("00")){
//            holder.statuspemesanan.setText("Menunggu konfirmasi pengelola");
//        }
        if(item.getStatuspemesanan().equals("00")){
            holder.statuspemesanan.setText("Menunggu konfirmasi pembayaran");
            holder.validasi.setVisibility(View.VISIBLE);
        }
        else if(item.getStatuspemesanan().equals("01")){
            holder.statuspemesanan.setText("Menunggu proses verifikasi");
            holder.validasi.setVisibility(View.GONE);
        }
        else if(item.getStatuspemesanan().equals("11")){
            holder.statuspemesanan.setText("Proses pemesanan tempat disetujui");
            holder.validasi.setVisibility(View.GONE);
        }

        return row;
    }

    static class ViewHolder {
        TextView namapemilik,jumlahharga,namalokasi,tipelokasi,jenislokasi,hargalokasi,nomorpemesanan,lamabooking,jumlahruangan,validasi,statuspemesanan;
        ImageView gambar;
    }
}