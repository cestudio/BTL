package ce.studio.btl;

public class GridItemLisLokasi {

    String idlokasi,namalokasi,gambar;

    public String getIdlokasi() {

        return idlokasi;

    }

    public void setIdlokasi(String idlokasi) {

        this.idlokasi = idlokasi;

    }

    public String getNamalokasi() {

        return namalokasi;

    }

    public void setNamalokasi(String namalokasi) {

        this.namalokasi = namalokasi;

    }

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }
}