package ce.studio.btl;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterListLokasi extends ArrayAdapter<GridItemLisLokasi> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemLisLokasi> mGridData = new ArrayList<GridItemLisLokasi>();

    public GridViewAdapterListLokasi(Context mContext, int layoutResourceId, ArrayList<GridItemLisLokasi> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemLisLokasi> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idlokasi = (TextView) row.findViewById(R.id.idlokasi);
            holder.namalokasi = (TextView) row.findViewById(R.id.namalokasi);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemLisLokasi item = mGridData.get(position);
        holder.idlokasi.setText(Html.fromHtml(item.getIdlokasi()));
        holder.namalokasi.setText(Html.fromHtml(item.getNamalokasi()));

        byte[] decodedString = Base64.decode(item.getGambar(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.gambar.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, 300, 300, false));
        return row;
    }

    static class ViewHolder {
        TextView idlokasi,namalokasi;
        ImageView gambar;
    }
}