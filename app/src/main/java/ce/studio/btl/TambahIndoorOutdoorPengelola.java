package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import ce.studio.btl.Galeri.RequestHandler;

public class TambahIndoorOutdoorPengelola extends AppCompatActivity {

    EditText jumlahruanganindoor,jumlahruanganoutdoor,sisaruanganindoor,sisaruanganoutdoor,hargaindoor,hargaoutdoor,deskripsilokasi;
    EditText fasilitasindoor1,fasilitasindoor2,fasilitasindoor3,fasilitasindoor4,fasilitasindoor5,fasilitasindoor6,fasilitasindoor7,fasilitasindoor8,fasilitasindoor9,fasilitasindoor10;
    EditText fasilitasoutdoor1,fasilitasoutdoor2,fasilitasoutdoor3,fasilitasoutdoor4,fasilitasoutdoor5,fasilitasoutdoor6,fasilitasoutdoor7,fasilitasoutdoor8,fasilitasoutdoor9,fasilitasoutdoor10;
    ImageView gambarindoor,gambaroutdoor;

    Button simpan;

    public static String FEED_URL;

    ProgressDialog pd;

    private Uri fileUri2,fileUri3;

    private String filePath = null;

    private static final String TAG = TambahIndoorOutdoorPengelola.class.getSimpleName();

    private int PICK_IMAGE_REQUEST = 1;

    private Bitmap bitmap2,bitmap3;

    String strimage2,strimage3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_indoor_outdoor_pengelola);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        hargaindoor = (EditText) findViewById(R.id.hargaindoor);
        hargaoutdoor = (EditText) findViewById(R.id.hargaoutdoor);
        jumlahruanganindoor = (EditText) findViewById(R.id.jumlahruanganindoor);
        jumlahruanganoutdoor = (EditText) findViewById(R.id.jumlahruanganoutdoor);
        sisaruanganindoor = (EditText) findViewById(R.id.sisaruanganindoor);
        sisaruanganoutdoor = (EditText) findViewById(R.id.sisaruanganoutdoor);
        deskripsilokasi = (EditText) findViewById(R.id.deskripsilokasi);

        fasilitasindoor1 = (EditText) findViewById(R.id.fasilitasindoor1);
        fasilitasindoor2 = (EditText) findViewById(R.id.fasilitasindoor2);
        fasilitasindoor3 = (EditText) findViewById(R.id.fasilitasindoor3);
        fasilitasindoor4 = (EditText) findViewById(R.id.fasilitasindoor4);
        fasilitasindoor5 = (EditText) findViewById(R.id.fasilitasindoor5);
        fasilitasindoor6 = (EditText) findViewById(R.id.fasilitasindoor6);
        fasilitasindoor7 = (EditText) findViewById(R.id.fasilitasindoor7);
        fasilitasindoor8 = (EditText) findViewById(R.id.fasilitasindoor8);
        fasilitasindoor9 = (EditText) findViewById(R.id.fasilitasindoor9);
        fasilitasindoor10 = (EditText) findViewById(R.id.fasilitasindoor10);

        fasilitasoutdoor1 = (EditText) findViewById(R.id.fasilitasoutdoor1);
        fasilitasoutdoor2 = (EditText) findViewById(R.id.fasilitasoutdoor2);
        fasilitasoutdoor3 = (EditText) findViewById(R.id.fasilitasoutdoor3);
        fasilitasoutdoor4 = (EditText) findViewById(R.id.fasilitasoutdoor4);
        fasilitasoutdoor5 = (EditText) findViewById(R.id.fasilitasoutdoor5);
        fasilitasoutdoor6 = (EditText) findViewById(R.id.fasilitasoutdoor6);
        fasilitasoutdoor7 = (EditText) findViewById(R.id.fasilitasoutdoor7);
        fasilitasoutdoor8 = (EditText) findViewById(R.id.fasilitasoutdoor8);
        fasilitasoutdoor9 = (EditText) findViewById(R.id.fasilitasoutdoor9);
        fasilitasoutdoor10 = (EditText) findViewById(R.id.fasilitasoutdoor10);

        gambarindoor = (ImageView) findViewById(R.id.gambarindoor);
        gambaroutdoor = (ImageView) findViewById(R.id.gambaroutdoor);
        simpan = (Button) findViewById(R.id.btnsimpan);

        gambarindoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionGrantedGaleri2();
            }
        });

        gambaroutdoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionGrantedGaleri3();
            }
        });

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImageGaleri();
            }
        });
    }

    private void showFileChooser2() {
        Intent intent = new Intent();
        filePath = "2";
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Ambil Gambar"), PICK_IMAGE_REQUEST);
    }

    private void showFileChooser3() {
        Intent intent = new Intent();
        filePath = "3";
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Ambil Gambar"), PICK_IMAGE_REQUEST);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST){
            if (resultCode == RESULT_OK && data != null && data.getData() != null) {

                if (filePath != null && filePath.equals("2")) {
                    previewMediaGaleri2(true,data);
                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }

                if (filePath != null && filePath.equals("3")) {
                    previewMediaGaleri3(true,data);
                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void previewMediaGaleri2(boolean isImage, Intent data) {
        if (isImage) {
            fileUri2 = data.getData();
            try {
                bitmap2 = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri2);
                gambarindoor.setImageBitmap(bitmap2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void previewMediaGaleri3(boolean isImage, Intent data) {
        if (isImage) {
            fileUri3 = data.getData();
            try {
                bitmap3 = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri3);
                gambaroutdoor.setImageBitmap(bitmap3);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public  boolean isStoragePermissionGrantedGaleri2() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                showFileChooser2();
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            showFileChooser2();
            return true;
        }
    }

    public  boolean isStoragePermissionGrantedGaleri3() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                showFileChooser3();
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            showFileChooser3();
            return true;
        }
    }

    public void uploadImageGaleri(){

        final String strhargaindoor = hargaindoor.getText().toString().trim();
        final String strhargaoutdoor = hargaoutdoor.getText().toString().trim();
        final String strjumlahruanganindoor = jumlahruanganindoor.getText().toString().trim();
        final String strjumlahruanganoutdoor = jumlahruanganoutdoor.getText().toString().trim();
        final String strsisaruanganindoor = sisaruanganindoor.getText().toString().trim();
        final String strsisaruanganoutdoor = sisaruanganoutdoor.getText().toString().trim();
        final String stridlokasi = getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("idlokasi","");
        final String stremail = getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("email","");
        final String strfasilitasindoor1 = fasilitasindoor1.getText().toString().trim();
        final String strfasilitasindoor2 = fasilitasindoor2.getText().toString().trim();
        final String strfasilitasindoor3 = fasilitasindoor3.getText().toString().trim();
        final String strfasilitasindoor4 = fasilitasindoor4.getText().toString().trim();
        final String strfasilitasindoor5 = fasilitasindoor5.getText().toString().trim();
        final String strfasilitasindoor6 = fasilitasindoor6.getText().toString().trim();
        final String strfasilitasindoor7 = fasilitasindoor7.getText().toString().trim();
        final String strfasilitasindoor8 = fasilitasindoor8.getText().toString().trim();
        final String strfasilitasindoor9 = fasilitasindoor9.getText().toString().trim();
        final String strfasilitasindoor10 = fasilitasindoor10.getText().toString().trim();
        final String strfasilitasoutdoor1 = fasilitasoutdoor1.getText().toString().trim();
        final String strfasilitasoutdoor2 = fasilitasoutdoor2.getText().toString().trim();
        final String strfasilitasoutdoor3 = fasilitasoutdoor3.getText().toString().trim();
        final String strfasilitasoutdoor4 = fasilitasoutdoor4.getText().toString().trim();
        final String strfasilitasoutdoor5 = fasilitasoutdoor5.getText().toString().trim();
        final String strfasilitasoutdoor6 = fasilitasoutdoor6.getText().toString().trim();
        final String strfasilitasoutdoor7 = fasilitasoutdoor7.getText().toString().trim();
        final String strfasilitasoutdoor8 = fasilitasoutdoor8.getText().toString().trim();
        final String strfasilitasoutdoor9 = fasilitasoutdoor9.getText().toString().trim();
        final String strfasilitasoutdoor10 = fasilitasoutdoor10.getText().toString().trim();
        if (fileUri2!=null){
            strimage2 = getStringImage(bitmap2);
        }
        if (fileUri3!=null){
            strimage3 = getStringImage(bitmap3);
        }

        class uploadImageGaleri extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = ProgressDialog.show(TambahIndoorOutdoorPengelola.this,"Tunggu sebentar.....","Mengunggah",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pd.dismiss();
                if(s.equals("000")){
                    Toast.makeText(TambahIndoorOutdoorPengelola.this, "Gagal mengubah fasilitas!", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(TambahIndoorOutdoorPengelola.this, "Sukses mengubah fasilitas!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> param = new HashMap<String,String>();
                param.put("idlokasi",stridlokasi);
                param.put("email",stremail);
                param.put("hargaindoor",strhargaindoor);
                param.put("hargaoutdoor",strhargaoutdoor);
                param.put("jumlahruanganindoor",strjumlahruanganindoor);
                param.put("jumlahruanganoutdoor",strjumlahruanganoutdoor);
                param.put("sisaruanganindoor",strsisaruanganindoor);
                param.put("sisaruanganoutdoor",strsisaruanganoutdoor);
                param.put("fasilitasindoor1",strfasilitasindoor1);
                param.put("fasilitasindoor2",strfasilitasindoor2);
                param.put("fasilitasindoor3",strfasilitasindoor3);
                param.put("fasilitasindoor4",strfasilitasindoor4);
                param.put("fasilitasindoor5",strfasilitasindoor5);
                param.put("fasilitasindoor6",strfasilitasindoor6);
                param.put("fasilitasindoor7",strfasilitasindoor7);
                param.put("fasilitasindoor8",strfasilitasindoor8);
                param.put("fasilitasindoor9",strfasilitasindoor9);
                param.put("fasilitasindoor10",strfasilitasindoor10);
                param.put("fasilitasoutdoor1",strfasilitasoutdoor1);
                param.put("fasilitasoutdoor2",strfasilitasoutdoor2);
                param.put("fasilitasoutdoor3",strfasilitasoutdoor3);
                param.put("fasilitasoutdoor4",strfasilitasoutdoor4);
                param.put("fasilitasoutdoor5",strfasilitasoutdoor5);
                param.put("fasilitasoutdoor6",strfasilitasoutdoor6);
                param.put("fasilitasoutdoor7",strfasilitasoutdoor7);
                param.put("fasilitasoutdoor8",strfasilitasoutdoor8);
                param.put("fasilitasoutdoor9",strfasilitasoutdoor9);
                param.put("fasilitasoutdoor10",strfasilitasoutdoor10);
                if (strimage2!=null){
                    param.put("image2",strimage2);
                }
                if (strimage3!=null){
                    param.put("image3",strimage3);
                }

                String result = rh.sendPostRequest(Config.TAMBAHINDOOROUTDOOR_URL, param);

                return result;
            }
        }
        uploadImageGaleri u = new uploadImageGaleri();
        u.execute();
    }
}
