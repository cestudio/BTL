package ce.studio.btl;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridViewAdapterListKomentar extends ArrayAdapter<GridItemListKomentar> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemListKomentar> mGridData = new ArrayList<GridItemListKomentar>();

    public GridViewAdapterListKomentar(Context mContext, int layoutResourceId, ArrayList<GridItemListKomentar> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemListKomentar> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.nama = (TextView) row.findViewById(R.id.nama);
            holder.email = (TextView) row.findViewById(R.id.email);
            holder.komentar = (TextView) row.findViewById(R.id.komentar);
            holder.tanggal = (TextView) row.findViewById(R.id.tanggal);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemListKomentar item = mGridData.get(position);
        holder.nama.setText(Html.fromHtml(item.getNama()));
        holder.email.setText(Html.fromHtml(item.getEmail()));
        holder.komentar.setText(Html.fromHtml(item.getKomentar()));
        holder.tanggal.setText(Html.fromHtml(item.getTanggal()));
        if(item.getJeniskomentar().equals("0")){
            holder.gambar.setBackgroundResource(R.drawable.unlike);
        }
        else{
            holder.gambar.setBackgroundResource(R.drawable.like);
        }

        return row;
    }

    static class ViewHolder {
        TextView nama,email,komentar,tanggal;
        ImageView gambar;
    }
}