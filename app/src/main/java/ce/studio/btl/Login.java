package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    Button btnregister,btnlogin;
    TextView btnlupapassword;

    private static final String TAG_RESULT = "result";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_IDUSER = "iduser";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_LEVEL = "level";
    private static final String TAG_TOKEN = "token";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    EditText username,password;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        btnlupapassword = (TextView) findViewById(R.id.btnlupapassword);
        btnregister = (Button) findViewById(R.id.btnregister);
        btnlogin = (Button) findViewById(R.id.btnlogin);

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this,Register.class));
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new login().execute();
            }
        });

        btnlupapassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LupaPassword lp = new LupaPassword(Login.this);
                lp.show();
            }
        });

    }

    private class login extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(Login.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.LOGIN_URL;
            FEED_URL += "?username="+username.getText().toString().replace(" ","%20");
            FEED_URL += "&password="+password.getText().toString().replace(" ","%20");
            FEED_URL += "&token="+getSharedPreferences("DATA",MODE_PRIVATE).getString("token","");

            Log.e("Login =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String login = a.getString(TAG_LOGIN);

                        if (login.equals("1")){
                            String iduser = a.getString(TAG_IDUSER);
                            String nama = a.getString(TAG_NAMA);
                            String email = a.getString(TAG_EMAIL);
                            String level = a.getString(TAG_LEVEL);
                            String token = a.getString(TAG_TOKEN);

                            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                            editor.putString("statlogin", "1");
                            editor.putString("iduser", iduser);
                            editor.putString("nama", nama);
                            editor.putString("email", email);
                            editor.putString("level", level);
                            editor.putString("token", token);
                            editor.commit();


                            if(level.equals("2")) {
                                Intent intent = new Intent(Login.this, HalamanUtamaPengelola.class);
                                startActivity(intent);
                                finish();
                            }
                            else if(level.equals("3")) {
                                Intent intent = new Intent(Login.this, HalamanUtama.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                        else{
                            Toast.makeText(Login.this, "Username/password salah!", Toast.LENGTH_SHORT).show();
                        }
                        pd.hide();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }
}
