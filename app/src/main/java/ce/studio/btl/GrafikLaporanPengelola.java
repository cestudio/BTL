package ce.studio.btl;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

public class GrafikLaporanPengelola extends AppCompatActivity {

    LineChart lineChart;

    ArrayList<String> xAXES;
    ArrayList<Entry> yAXESjumlah;
    ArrayList<Entry> yAXESkelembaban;

    public static String FEED_URL;

    private static final String TAG = GrafikLaporanPengelola.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafik_laporan_pengelola);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lineChart = (LineChart) findViewById(R.id.linechart);

        FEED_URL = Config.LAPORANGRAFIK_URL;
        FEED_URL += "?idlokasi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idlokasi","");
        FEED_URL += "&kategori="+getSharedPreferences("DATA",MODE_PRIVATE).getString("kategori","");

        xAXES = new ArrayList<>();
        yAXESjumlah = new ArrayList<>();
        yAXESkelembaban = new ArrayList<>();

        new AsyncHttpTask().execute(FEED_URL);
    }

    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {

            Integer result = 0;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1;
                } else {
                    result = 0;
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                String[] xaxes = new String[xAXES.size()];
                for(int i=0; i<xAXES.size();i++){
                    xaxes[i] = xAXES.get(i).toString();
                }

                ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

                LineDataSet lineDataSet1 = new LineDataSet(yAXESjumlah,"Jumlah pemesanan");
                lineDataSet1.setDrawCircles(true);
                lineDataSet1.setColor(Color.RED);
//                lineDataSet1.setDrawCubic(true);
                lineDataSet1.setDrawFilled(true);
                lineDataSet1.setFillColor(Color.RED);

                lineDataSets.add(lineDataSet1);

                lineChart.setData(new LineData(xaxes,lineDataSets));

                    lineChart.setVisibleXRangeMaximum(65f);

                lineChart.setDescription("Laporan "+getSharedPreferences("DATA",MODE_PRIVATE).getString("kategori",""));

                lineChart.animateY(5000);

            } else {
//                Toast.makeText(RekamData.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {

        Log.e("tes : ",result);

        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");

            yAXESjumlah.add(new Entry(0,0));
            xAXES.add(0, "Tanggal");

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String jumlah = post.optString("jumlahruangan");
                String tanggal = post.optString("tanggal");

                yAXESjumlah.add(new Entry(Float.parseFloat(jumlah),i+1));
                xAXES.add(i+1, tanggal);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
