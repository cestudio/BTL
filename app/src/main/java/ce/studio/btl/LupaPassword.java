package ce.studio.btl;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by ASUS on 2/5/2017.
 */
public class LupaPassword extends Dialog {
    public Activity c;
    public Button kirim;
    public EditText username;

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    String passwordbaru;

    public LupaPassword(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layoutlupapassword);
        username = (EditText) findViewById(R.id.username);
        kirim = (Button) findViewById(R.id.btnkirim);

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passwordbaru = randomString(CHARSET_AZ_09,4);

                new lupapassword().execute();
            }
        });


    }

    public static String randomString(char[] characterSet, int length) {
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }

    private class lupapassword extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {

            pd = new ProgressDialog(getContext());
            pd.setMessage("Sedang mengirim password baru, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            try {

                FEED_URL = Config.LUPAPASSWORD;
                FEED_URL += "?username="+username.getText().toString().replace(" ","%20");
                FEED_URL += "&password="+passwordbaru;

                Log.e("Lupa Password =",FEED_URL);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(FEED_URL);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }


            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("0")){
                Toast.makeText(getContext(), "Gagal mereset password!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }
            else if(result.equals("1")){
                SharedPreferences.Editor editor = c.getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("username", username.getText().toString());
                editor.commit();

                Intent intent = new Intent(getContext(), PasswordBaru.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(getContext(),0 /* request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                long[] pattern = {500,500,500,500,500};

                Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle("Lupa Password BTL")
                        .setContentText("Password baru anda adalah "+passwordbaru)
                        .setAutoCancel(true)
                        .setVibrate(pattern)
                        .setLights(Color.BLUE,1,1)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                Toast.makeText(getContext(), "Sukses mereset password!", Toast.LENGTH_SHORT).show();
                dismiss();
                pd.hide();
            }
            else if(result.equals("2")){
                Toast.makeText(getContext(), "Username tidak terdaftar!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }

            super.onPostExecute(result);
        }

    }
}
