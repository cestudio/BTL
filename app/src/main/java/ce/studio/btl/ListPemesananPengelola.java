package ce.studio.btl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class ListPemesananPengelola extends AppCompatActivity {

    private static final String TAG = ListPemesananPengelola.class.getSimpleName();
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterListPemesanan mGridAdapter;
    private ArrayList<GridItemListPemesanan> mGridData;
    private static final String TAG_NAMAPEMILIK = "namapemilik";
    private static final String TAG_JUMLAHHARGA = "jumlahharga";
    private static final String TAG_NAMALOKASI = "namalokasi";
    private static final String TAG_TIPELOKASI = "tipelokasi";
    private static final String TAG_HARGALOKASI = "hargalokasi";
    private static final String TAG_NOMORPEMESANAN = "nomorpemesanan";
    private static final String TAG_LAMABOOKING = "lamabooking";
    private static final String TAG_JUMLAHRUANGAN = "jumlahruangan";
    private static final String TAG_DISKON = "diskon";
    private static final String TAG_STATUSPEMESANAN = "statuspemesanan";

    ProgressDialog pd;

    private static final String TAG_RESULT = "result";
    private static final String TAG_KONFIRMASI = "konfirmasi";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pemesanan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FEED_URL = "http://";
        FEED_URL = Config.LISTPEMESANANPENGELOLA_URL;
        FEED_URL += "?iduser="+getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("iduser","");

        mGridView = (GridView) findViewById(R.id.gridView);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterListPemesanan(ListPemesananPengelola.this, R.layout.grid_item_layout_list_pemesanan, mGridData);
        mGridView.setAdapter(mGridAdapter);

        pd = new ProgressDialog(ListPemesananPengelola.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        //Start download
        new AsyncHttpTask().execute(FEED_URL);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemListPemesanan item = (GridItemListPemesanan) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idpemesanan", item.getNomorpemesanan());
                editor.putString("kategori", item.getTipelokasi());
                editor.commit();

                if(item.getStatuspemesanan().equals("110")){
                    Toast.makeText(ListPemesananPengelola.this, "Anda sudah melakukan konfirmasi pemesanan ini!", Toast.LENGTH_SHORT).show();
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListPemesananPengelola.this);
                    builder.setMessage("Apa anda ingin mengonfirmasi pemesanan?");
                    builder.setCancelable(true);

                    builder.setPositiveButton(
                            "Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    new konfirmasi().execute();
                                }
                            });

                    builder.setNegativeButton(
                            "Tidak",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alert1 = builder.create();
                    alert1.show();
                }

            }
        });
    }

    private class konfirmasi extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(ListPemesananPengelola.this);
            pd.setMessage("Sedang mengonfirmasi pemesanan, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
//            FEED_URL = "http://";
//            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL = Config.KONFIRMASIPEMESANAN_URL;
            FEED_URL += "?idpemesanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idpemesanan","");

            Log.e("Konfirmasi =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String konfirmasi = a.getString(TAG_KONFIRMASI);

                        if (konfirmasi.equals("1")){
                            pd.hide();
                            Toast.makeText(ListPemesananPengelola.this, "Berhasil melakukan konfirmasi!", Toast.LENGTH_SHORT).show();
                            FEED_URL = Config.LISTPEMESANANPENGELOLA_URL;
                            FEED_URL += "?iduser="+getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("iduser","");

                            mGridView = (GridView) findViewById(R.id.gridView);

                            //Initialize with empty data
                            mGridData = new ArrayList<>();
                            mGridAdapter = new GridViewAdapterListPemesanan(ListPemesananPengelola.this, R.layout.grid_item_layout_list_pemesanan, mGridData);
                            mGridView.setAdapter(mGridAdapter);

                            pd = new ProgressDialog(ListPemesananPengelola.this);
                            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
                            pd.setCancelable(false);
                            pd.show();

                            //Start download
                            new AsyncHttpTask().execute(FEED_URL);
                        }
                        else{
                            Toast.makeText(ListPemesananPengelola.this, "Gagal melakukan konfirmasi!", Toast.LENGTH_SHORT).show();
                            pd.hide();
                        }
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemListPemesanan item;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String strnamapemilik = post.getString(TAG_NAMAPEMILIK);
                String strjumlahharga = post.getString(TAG_JUMLAHHARGA);
                String strnamalokasi = post.getString(TAG_NAMALOKASI);
                String strtipelokasi = post.getString(TAG_TIPELOKASI);
                String strhargalokasi = post.getString(TAG_HARGALOKASI);
                String strnomorpemesanan = post.getString(TAG_NOMORPEMESANAN);
                String strlamabooking = post.getString(TAG_LAMABOOKING);
                String strjumlahruangan = post.getString(TAG_JUMLAHRUANGAN);
//                String strdiskon = post.getString(TAG_DISKON);
                String strstatuspemesanan = post.getString(TAG_STATUSPEMESANAN);
                item = new GridItemListPemesanan();
                item.setNamapemilik(strnamapemilik);
                item.setJumlahharga("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strjumlahharga))).replace(",","."));
                item.setNamalokasi(strnamalokasi);
                item.setTipelokasi(strtipelokasi);
                item.setHargalokasi("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strhargalokasi))).replace(",","."));
                item.setNomorpemesanan(strnomorpemesanan);
                item.setLamabooking(strlamabooking);
                item.setJumlahruangan(strjumlahruangan);
//                item.setDiskon(strdiskon);
                item.setStatuspemesanan(strstatuspemesanan);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString("gambarlokasi"));
                }

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
